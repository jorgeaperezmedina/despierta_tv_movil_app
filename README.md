# News App

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


### Plugins run in Androidx


## Android Realease

Android 9 config in AndroidManifest.xml

```
    android:usesCleartextTraffic="true"
```

Like This Example:

```xml
<application
        android:name="io.flutter.app.FlutterApplication"
        android:label="news_app_movil"
        android:usesCleartextTraffic="true"
        android:icon="@mipmap/ic_launcher">
```

Modify build.gradle

```
  signingConfigs {
        release {
            keyAlias 'keyAlias'
            keyPassword 'keyPassword'
            storeFile file('despierta_tv.keystore')
            storePassword 'storePassword'
        }
    }

    buildTypes {
        release {
            // TODO: Add your own signing config for the release build.
            // Signing with the debug keys for now, so `flutter run --release` works.
            signingConfig signingConfigs.release
        }
    }
```

Create file 64 bits

```bash
  flutter build apk --target-platform=android-arm64
```

Create file 32 bits

```bash
  flutter build apk --target-platform=android-arm
```

## iOS
    cambio del lenguaje Swift o kotlin se tiene que ejecutar el comando flutter create <nombre_proyecto> a fuera de la ubicacion del proyecto y eliminar la carpeta del codigo a remplazar ya sea android o IOS.


## iOS 12 or less
  Active to consume apis without https

  In info.plist
  ```
    <key>NSAppTransportSecurity</key>
    <dict>
      <key>NSAllowsArbitraryLoads</key>
      <true/>
    </dict>
  ```

### iOs Release
  Subir IOS en Appstore:

  Markup : 1. updated my iOS toolchain with flutter doctor or change channel to stable for flutter
           2. verify to open proyect with the Runner.xcodeworkspace in xcode
           3. Run in terminal
            * flutter clean
            * flutter build ios
           4. Archive in Xcode with schema select Generic ios Device
           5. Validate app and after distribute app



### Notifications
  ## Android
  In android add the google.json in path android/app/google.json
    Cambiar las llaves del parse en el AndroidManifest manualmente
    ```
      <meta-data android:name="com.parse.push.notification_icon" android:resource="@drawable/ic_notification"/>
      <meta-data android:name="ParseAppId" android:value="ParseAppIdVALUE" />
      <meta-data android:name="ParseClientKey" android:value="ParseClientKeyVALUE" />
      <meta-data android:name="ParseServerUrl" android:value="ParseServerUrlVALUE" />
    ```
    Active the google services
     More info in the plugin notification


  ## iOS
  In Ios active the capabilities
    Push Notifications
    Background Modes -> Remote Notifications

  Is required to generate the certicate apns to receive push notifications, only receive when use a phisical device.
  More info in the plugin notification


## data/mediaflix/
  modelos usando la estructura de graphql con el sistema Mediaflix.

  ## Flutter version
  la version de flutter utilizada para este proyecto:
  1.9.1+hotfix.3 channel stable

  ###Nota
  No se pudo realizar la firma de los apks mediante flutter ocacionando un error al momento de usar las llaves.
  Se uso la herramienta Android Studio para la firma de los apks mediante el uso de las llaves.
  Android Studio https://developer.android.com/studio/?gclid=CjwKCAiAmNbwBRBOEiwAqcwwpUW7K5iqWTXFAXaw4ry8ZfyJXnqlFIuFqi8gxf_8b8Y-95joPAWONRoC7rEQAvD_BwE

  ### Nota
  No subir el archivo flutter_export_enviroment.sh
  causara problemas a los compañeros por las ruta donde se creara el Runner.ipa
  .
