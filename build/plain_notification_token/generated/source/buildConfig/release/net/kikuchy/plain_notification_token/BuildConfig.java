/**
 * Automatically generated file. DO NOT MODIFY
 */
package net.kikuchy.plain_notification_token;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "net.kikuchy.plain_notification_token";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
