import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';
import 'package:chewie/chewie.dart';
import 'package:news_app/libraries.dart';
import 'package:auto_orientation/auto_orientation.dart';
import 'package:screen/screen.dart';
import 'package:connectivity/connectivity.dart';

class VideoWidget extends StatefulWidget {
  final String urlVideo;
  final bool isLiveStream;
  final Duration videoDuration;
  final String title;
  final List<dynamic> videoQualty;
  VideoWidget(this.urlVideo, this.isLiveStream, this.videoDuration, this.title,
      this.videoQualty);
  @override
  State<StatefulWidget> createState() {
    return _VideoWidgetState();
  }
}

bool fullScreenRefresh = false;

class _VideoWidgetState extends State<VideoWidget> {
  VideoPlayerValue _playerValue;
  ChewieController _chewieController;
  VideoPlayerController videoPlayerController;

  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool restart;
  bool isFailPlayyer;
  bool orientation;
  @override
  void initState() {
    super.initState();
    if (widget.isLiveStream == true) {
    } else {
      setLandscapeOrientation();
      AutoOrientation.landscapeMode();
    }

    if (orientation == null) {
      orientation = false;
    }
    closeOption();

    ///Se inicializa la funcion initCOnnectivity para sabaer el status de conexion del dispositivo.
    ///Creando un listen a la funcion onConnectivityChanged  usando la funcion _updateConnectionStatus como parametro.
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    Screen.keepOn(true);

    ///Se inicializa el videoPlayerController añadiendo la url como parametro.
    videoPlayerController = VideoPlayerController.network(widget.urlVideo);

    ///Se inicializa el _chewieController Con la configuracion necesaria para la creacion del estilo del video player.
    _chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: true,
      isLive: widget.isLiveStream,
      aspectRatio: 16 / 9,

      ///Se agrega el titulo como parametro string para ser mostrado adentro del video player.
      titleVideo: widget.title,

      /// Se agrega una lista de urls para la opcion de cambio de calidad del video.
      videoQuality: widget.videoQualty,

      ///DurationSpecific es un parametro de tipo Duration, en la cual dependiendo si es un video liveStream la duracion sera cero
      ///Si es un video VOD se le pasara la duracion en segundos.
      durationSpecific:
          widget.isLiveStream == true ? Duration.zero : widget.videoDuration,

      ///El parametro allowFullScreen cuenta con una funcion CloseOption la cual valida si el sistema es IOS y si es un liveStream o un video VOD.
      allowFullScreen: closeOption(),
      fullScreenByDefault: orientation,

      /// el parametro closeButton  recive una pequeña validacion con valore booleanos que dependiendo del sistema si es IOS se mostrara el boton de close de lo contrario
      /// no se mostrara nada.
      closeButton: Platform.isIOS ? true : false,
      routePageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondAnimation, provider) {
        return AnimatedBuilder(
            animation: animation,
            builder: (BuildContext context, Widget child) {
              return VideoScaffold(
                urlVideo: widget.urlVideo,
                child: Scaffold(
                    resizeToAvoidBottomPadding: false,
                    body: Container(
                      color: Colors.black,
                      child: provider,
                    )),
              );
            });
      },
    );

    ///Esta funcion inicializa el listener hacia el video player.
    _initialize();
  }

  ///Esta funcio retorna un valor booleano el cual es validado para sistemas IOS y si es un video liveStream o VOD.
  closeOption() {
    if (Platform.isIOS == true && widget.isLiveStream == true ||
        Platform.isIOS == false && widget.isLiveStream == true) {
      return true;
    } else if (Platform.isIOS == true && widget.isLiveStream == false) {
      return false;
    } else if (Platform.isIOS == false && widget.isLiveStream == false) {
      return true;
    }
  }

  /// Esta funcion asincrona retorna un resultado con los valores de conexion del dispositivo.
  Future<void> initConnectivity() async {
    ConnectivityResult result;

    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    if (!mounted) {
      return;
    }
    _updateConnectionStatus(result);
  }

  @override
  Widget build(BuildContext context) {
    /// Se envia a la funcion durationChewie la duracion del video al video player, para solucionar el error de la barra de duracion.

    var orientationDevices =
        MediaQuery.of(context).orientation == Orientation.portrait;

    if (orientationDevices == false) {
      setState(() {
        orientation = true;
      });
    } else {
      setState(() {
        orientation = false;
      });
    }

    _chewieController.videoPlayerController
        .durationChewie(widget.videoDuration);

    setState(() {});

    /// Se crea una validacion para el tipo de orientacion PortainMode para los video que son LiveStream y setLandscapeOreintation para videos VOD.
    widget.isLiveStream == true
        ? AutoOrientation.portraitMode()
        : setLandscapeOrientation();
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      body: Container(
        alignment: Alignment(0, 0),
        child: validationBody(),
      ),
    );
  }

  ///Esta funcion valida si el video player sufre algun error, si sufre un error retorna la funcion buttonRefresh
  ///si no retorna una funcion la cual contiene el chewie.
  validationBody() {
    if (_playerValue.hasError == true || restart) {
      return buttonRefreh();
    } else {
      return chewiePlayerCreate();
    }
  }

  ///Esta funcion retorna el widget chewie, con su respectivo controlador.
  chewiePlayerCreate() {
    return Chewie(controller: _chewieController);
  }

  /// Esta funcion asincrona actualiza los valores dependiendo de los resultados de conexion del dispisitivo.
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = result.toString();
        });
        break;
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() {
          _connectionStatus = result.toString();
        });
        break;
      default:
        setState(() {
          _connectionStatus = 'File to get Connectivity';
        });
    }
  }

  /// En este widget se retorna un IconButton el cual esta validado para retornar una funcion _settingModalBottomSheet si el dispositivo no tiene conexion
  /// y si el dispositivo esta conectado a una red wifi o usa sus datos moviles retorna el IconButton con la funcion de recarga del video player y el chewie.
  Widget buttonRefreh() {
    return Center(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.black, borderRadius: BorderRadius.circular(40.0)),
        child: IconButton(
          icon: Icon(Icons.refresh),
          iconSize: 40.0,
          color: Colors.white,
          onPressed: () {
            if (_connectionStatus == 'ConnectivityResult.none' &&
                _playerValue.hasError == true) {
              return _settingModalBottomSheet(context);
            } else if (_playerValue.hasError == true ||
                _connectionStatus != 'ConnectivityResult.none' ||
                fullScreenRefresh == true) {
              refreshValue2();
            } else if (_connectionStatus == 'ConnectivityResult.none') {
              return _settingModalBottomSheet(context);
            }
          },
        ),
      ),
    );
  }

  ///Esta funcion retorna una lista de titles.
  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: 50,
            alignment: Alignment.center,
            color: Color(0XFFff0000),
            child: new Wrap(
              alignment: WrapAlignment.center,
              children: <Widget>[
                new ListTile(
                    title: new Text(
                      'Sin Conexion',
                      style: TextStyle(color: Colors.white),
                    ),
                    onTap: () => {}),
              ],
            ),
          );
        });
  }

  ///Esta funcion hace cambios en los valores en el videoPlayerController y el ChewieController, eliminando el anterior chewiecontroller
  ///y creando otro.
  refreshValue2() {
    setState(() {
      closeOption();
      _chewieController.dispose();
      videoPlayerController.pause();
      videoPlayerController.seekTo(Duration.zero);
      videoPlayerController = VideoPlayerController.network(widget.urlVideo);
      _chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 16 / 9,
        autoPlay: true,
        isLive: widget.isLiveStream,
        durationSpecific: widget.videoDuration,
        allowFullScreen: closeOption(),
        titleVideo: widget.title,
        videoQuality: widget.videoQualty,
        closeButton: Platform.isIOS ? true : false,
      );
      _initialize();
    });
  }

  ///Esta funcion hace una validacion para retornar un cambio en la variable isFailPlayyer.
  changeValuePlayer() {
    if (_playerValue.hasError == null) {
      isFailPlayyer = false;
    } else if (_playerValue.hasError == true) {
      isFailPlayyer = true;
    } else if (_playerValue.hasError == false) {
      isFailPlayyer = false;
    }
  }

  /// La funcion asyncrona _initialize añade un listener al video controller reciviendo como parametro una funcion _updateState la cual
  /// mantiene un constante monitoreo a los cambio en el video player.
  Future<Null> _initialize() async {
    videoPlayerController.addListener(_updateState);

    _updateState();
  }

  ///Esta funcio ejecuta un setState el cual envia los cambios nuevos de los valores del player y crea una validacion
  ///para el cierre del la pantalla completa en caso de que ocurra un error.
  void _updateState() {
    setState(() {
      _playerValue = videoPlayerController.value;
      changeValuePlayer();
      if (isFailPlayyer == true && _chewieController.isFullScreen == true ||
          _connectionStatus == 'ConnectivityResult.none' &&
              _chewieController.isFullScreen ||
          restart == true && _chewieController.isFullScreen) {
        _chewieController.exitFullScreen();
      }
      if (widget.isLiveStream == true) {
        setState(() {
          restart = false;
        });
      } else if (widget.isLiveStream == false) {
        setState(() {
          restart = _playerValue.position >= _chewieController.durationSpecific;
        });
      }
    });
  }

  @override
  void dispose() async {
    videoPlayerController.dispose();
    _chewieController.dispose();
    _connectivitySubscription.cancel();
    setPortraitOrientation();
    AutoOrientation.portraitMode();
    Screen.keepOn(false);
    super.dispose();
  }
}

class VideoScaffold extends StatefulWidget {
  const VideoScaffold({Key key, this.child, this.urlVideo}) : super(key: key);
  final Widget child;
  final String urlVideo;

  State<StatefulWidget> createState() => _VideoScaffoldState();
}

class _VideoScaffoldState extends State<VideoScaffold> {
  @override
  void initState() {
    setLandscapeOrientation();
    AutoOrientation.landscapeMode();
    super.initState();
  }

  @override
  void dispose() {
    setPortraitOrientation();
    AutoOrientation.portraitMode();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: widget.child,
    );
  }
}
