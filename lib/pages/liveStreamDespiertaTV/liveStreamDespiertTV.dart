import 'dart:async';
import 'dart:io';
import 'package:connection_status_bar/connection_status_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dio/dio.dart';
import 'package:news_app/data/mediaflix/schedules.dart';
import 'package:news_app/controllers/schedules_controller.dart';
import 'package:news_app/filters.dart';
import 'package:screen/screen.dart';
import 'package:news_app/queries/schedule.dart' as Queries;
import 'package:news_app/queries/livestream.dart' as Queries;
import 'package:news_app/pages/video_player/video_test.dart';
import 'package:news_app/controllers/live_stream_controller.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:news_app/widgets/menu_left/index.dart';
import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexLiveStreamDT extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StateIndexLiveStream();
  }
}

class StateIndexLiveStream extends State<IndexLiveStreamDT> {
  Schedules currentSchedule = Schedules();
  bool isLoading = false;
  int page = 1;
  Dio dio = new Dio();
  List<Schedules> schedules = new List<Schedules>();
  List<Schedules> firstSchedule = new List<Schedules>();
  ScrollController listview = new ScrollController();
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  bool showButton;
  Timer _timer;
  bool darkMode;

  @override
  void initState() {
    /// Se inicializa el widget de Connectivity para saber el estado de conexion del dispositivo.
    initConnectivity();

    ///se crea una variable para añadir un listen a una funcion con parametro una funcion para actualiarsu estatus.
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    _timer = Timer(Duration(seconds: 15), () {
      setState(() {
        showButton = true;
      });
    });
    Screen.keepOn(true);

    /// Se inicializa la funcion para la peticion de datos.
    _loadDataEvents();
    getDarkTest();
    super.initState();
  }

  /// Esta funcion crea una respuesta y la almacena en la variable result.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    if (!mounted) {
      return;
    }
    _updateConnectionStatus(result);
  }

  getDarkTest() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getBool('isDark') == null) {
      darkMode = false;
    } else {
      darkMode = sharedPreferences.getBool('isDark');
      print('valor de isDark');
      print(darkMode);
    }
    Globals.themeMode = darkMode;
  }

  @override
  Widget build(BuildContext context) {
    print(Globals.titleLiveStream);
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: darkMode == true
              ? Color(Globals.darkThemeAppBar)
              : Color(Globals.lightThemeAppBar),
          title: Text(
            Globals.title,
            style: TextStyle(color: Colors.white),
          ),
        ),
        drawer: MenuLeft(),
        backgroundColor: darkMode == true
            ? Color(Globals.darkThemeBackground)
            : Color(Globals.lightThemeBackgroundLive),

        ///Se crea una validacion, si el dispositivo tiene conexion retorna una funcion llamada loadData de lo contrario retorna el contenido de la vista.
        body: _connectionStatus == 'ConnectivityResult.none' &&
                    schedules.isEmpty &&
                    isLoading == true ||
                schedules.isEmpty ||
                Globals.liveStream == null
            // ? loadData()
            ? columnBody()
            : columnBody());
  }

  /// En esta funcion actualiza el estatus mediante el parametro de tipo ConnectivityResult y un switch ejecutando un setState
  /// para añadir un valor string a la variable _connectionStatus.
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = result.toString();
        });
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = result.toString();
        });
        break;
      case ConnectivityResult.none:
        setState(() {
          _connectionStatus = result.toString();
        });
        break;
      default:
        setState(() {
          _connectionStatus = 'Fail to get Connectivity';
        });
    }
  }

  /// Esta funcion retorna un widget column que contiene un  widget flexible que retorna un container con el videoPlayer
  ///  y un listView con la programacion del dia.
  Widget columnBody() {
    return Column(
      children: <Widget>[
        /// esta funcion retorna una alerta para los sistemas IOS.
        alertConnectionIOS(),
        Flexible(
            flex: 1,
            child: Container(
              color: Colors.black,
              padding: EdgeInsets.only(top: 0),
              child: FutureBuilder(
                future: getLiveStream(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  print(snapshot);
                  if (Globals.liveStream != '' &&
                      Globals.liveStream != null &&
                      Globals.liveStream.length > 0) {
                    return VideoWidget(Globals.liveStream, true, Duration.zero,
                        Globals.titleLiveStream, []);
                  }
                  return _progressIndicator();
                },
              ),
            )),
        Container(
            margin: EdgeInsets.only(bottom: 5.0), child: _iconFirstSchedule()),
        Expanded(
          child: schedules == null || schedules.isEmpty
              ? Align(
                  alignment: Alignment.center,
                  child: Text(
                    'Sin contenido',
                    style: TextStyle(
                        color: darkMode == true
                            ? Color(Globals.darkThemeText).withOpacity(0.5)
                            : Color(Globals.lightThemeText).withOpacity(0.5)),
                  ),
                )
              : _listShcedules(),
        )
      ],
    );
  }

  ///Esta funcion retorna una alerta si el dispositivo se encuentra desconectado de internet.
  ///y se valida que sea solo para dispositivos con sistema IOS.
  alertConnectionIOS() {
    if (Platform.isIOS == true &&
        _connectionStatus == 'ConnectivityResult.none') {
      return Container(
        color: Colors.black,
        child: ConnectionStatusBar(
          height: 25, // double: default height
          width: double.maxFinite, // double: default width
          color: Color(0XFFff0000), // Color: default background color
          lookUpAddress:
              'google.com', // String: default site to look up for checking internet connection
          endOffset: const Offset(
              0.0, 0.0), // Offset: default animation finish point offset
          beginOffset: const Offset(
              0.0, -1.0), // Offset: default animation start point offset
          animationDuration: const Duration(
              milliseconds: 200), // Duration: default animation duration
          // Text: default text
          title: const Text(
            'Sin conexion',
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        ),
      );
    } else if (Platform.isIOS == true &&
        _connectionStatus != 'ConnectivityResult.none') {
      return Container(
        color: Colors.black,
      );
    } else if (Platform.isIOS == false) {
      return Container(
        color: Colors.black,
      );
    }
  }

  Widget loadData() {
    return Container(
      color: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),
      child: timer(),
    );
  }

  timer() {
    if (showButton == true) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[iconError(), _circularProgress(), buttonRefresh()],
        ),
      );
    } else {
      setState(() {
        showButton = false;
      });
      return Center(
        child: Platform.isIOS
            ? CupertinoActivityIndicator(
                radius: 20.0,
              )
            : CircularProgressIndicator(),
      );
    }
  }

  Widget iconError() {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Icon(
        Icons.error_outline,
        color: Colors.white,
        size: 40,
      ),
    );
  }

  Widget _circularProgress() {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Platform.isIOS
          ? CupertinoActivityIndicator(
              radius: 20.0,
            )
          : CircularProgressIndicator(),
    );
  }

  Widget textError() {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Text(
        '',
        style: TextStyle(color: Colors.white, fontSize: 30),
      ),
    );
  }

  Widget buttonRefresh() {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: RaisedButton(
        //  color: darkMode == true ? Color(Globals.darkThemeBackground) :  Color(Globals.lightThemeBackground),
        child: Text(
          'Reintentar',
          style: TextStyle(
              fontSize: 17,
              color: darkMode == true
                  ? Color(Globals.darkThemeTextTags)
                  : Color(Globals.lightThemeTextTags)),
        ),
        onPressed: () {
          setState(() {
            validationAction();
          });
        },
      ),
    );
  }

  ///En esta funcion se retorna la funcion getSchedules el cual requiere de los parametros  Dio, la querie de consulta.
  Future<Null> _refresh() async {
    ///Parametros requeridos para la query de consulta.
    Map<String, dynamic> params = {"slug": "livetv", "platform": "movil"};

    /// Se hace la uso de la funcion del controlador para hacer la cosulta.
    return await getSchedules(dio, params, Queries.getCurrentLive).then((data) {
      ///Se valida si hay algun dato vacio o nulo al momento de regresar la peticion.
      if (data != null && data != []) {
        firstSchedule.add(data[0]);
        schedules.addAll(data);
        schedules.remove(schedules[0]);
        setState(() {
          isLoading = false;
        });
      }
    });
  }

  ///Se valida y se cambio el valor de la variable showButton mediante el uso de la variable _connectionStatus.
  validationAction() {
    if (_connectionStatus == 'Connectivity.none' && schedules.isEmpty ||
        schedules.contains(null)) {
      setState(() {
        _timer = Timer(Duration(seconds: 5), () {
          setState(() {
            showButton = true;
          });
        });
      });
    } else if (_connectionStatus != 'ConnectivityResult.none') {
      setState(() {
        showButton = false;
        _refresh();
      });
    }
  }

  ///Esta funcion se encarga de la peticion de datos mediante el uso del modelo, controlador, para obtener la url del liveStream.
  Future<void> getLiveStream() async {
    if (Globals.liveStream != '' &&
        Globals.liveStream != null &&
        Globals.liveStream.length > 0) {
      return true;
    }
    print("get live ");
    Map<String, dynamic> params = {"slug": "livetv", "platform": "movil"};
    await getLiveStreamGraphql(dio, params, Queries.getLive).then((data) {
      print("ive ");
      if (data != null) {
        Globals.liveStream = data.url;
      }
    });
  }

  Widget _iconFirstSchedule() {
    return Container(
      width: double.infinity,
      color: darkMode == true
          ? Color(Globals.darkThemeCards)
          : Color(Globals.lightThemeBackgroundLive),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10),
            alignment: Alignment.topLeft,
            height: 70,
            child: darkMode == true
                ? Image.asset(Globals.darkThemeLogo)
                : Image.asset(Globals.logo),
          ),
          Wrap(
            direction: Axis.vertical,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 1.5,
                alignment: Alignment.topCenter,
                margin: EdgeInsets.all(5.0),
                child: Text(
                  firstSchedule.isEmpty ? '' : 'Estas Viendo ...',
                  style: TextStyle(
                      fontSize: 18,
                      color: darkMode == true
                          ? Color(Globals.darkThemeText)
                          : Color(Globals.lightThemeText),
                      fontWeight: FontWeight.bold),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.4,
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(right: 5.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: firstSchedule.isEmpty
                      ? Text(
                          'Sin contenido',
                          style: TextStyle(
                              fontSize: 15,
                              color: darkMode == true
                                  ? Color(Globals.darkThemeText)
                                      .withOpacity(0.5)
                                  : Color(Globals.lightThemeText)
                                      .withOpacity(0.5),
                              fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        )
                      : Text(
                          firstSchedule[0].title,
                          style: TextStyle(
                              fontSize: 15,
                              color: darkMode == true
                                  ? Color(Globals.darkThemeText)
                                  : Color(Globals.lightThemeText),
                              fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  testButton() {
    if (schedules.isEmpty && _connectionStatus == 'ConnectivityResult.none' ||
        _connectionStatus != 'ConnectivityResult.none' && schedules.isEmpty) {
      return Expanded(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(20),
          children: <Widget>[
            RaisedButton(
              color: darkMode == true
                  ? Color(Globals.darkThemeCards)
                  : Color(Globals.lightThemeCards),
              child: Text(
                'Recargar',
                style: TextStyle(color: Colors.white, fontSize: 17),
              ),
              onPressed: () {
                validationAction();
              },
            )
          ],
        ),
      );
    } else {
      return Expanded(
        child: _listShcedules(),
      );
    }
  }

  Future _refreshData() async {
    schedules = [];
    firstSchedule = [];
    showButton = false;
    validationAction();
    _loadDataEvents();
  }

  Widget _listShcedules() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: schedules.length,
      itemBuilder: (BuildContext context, int index) {
        if (index == schedules.length) {
          return _progressIndicator();
        } else {
          return _cards(schedules[index]);
        }
      },
    );
  }

  Widget _cards(Schedules schedules) {
    return Column(children: [
      Container(
        color: darkMode == true
            ? Color(Globals.darkThemeCards)
            : Color(Globals.lightThemeCards),
        margin: EdgeInsets.only(left: 5, right: 5, bottom: 5),
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: EdgeInsets.all(4),
                child: Text(
                  schedules.title,
                  style: TextStyle(
                    fontSize: 15,
                    color: darkMode == true
                        ? Color(Globals.darkThemeText)
                        : Color(Globals.lightThemeText),
                  ),
                  textAlign: TextAlign.left,
                )),
            Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  child: Text(
                    parseDateTimeSchedules(schedules.beginTime),
                    style: TextStyle(
                      fontSize: 15,
                      color: darkMode == true
                          ? Color(Globals.darkThemeTextSchedule)
                          : Color(Globals.lightThemeTextSchedule),
                    ),
                  ),
                ),
                Text(
                  '_',
                  style: TextStyle(
                    color: darkMode == true
                        ? Color(Globals.darkThemeTextSchedule)
                        : Color(Globals.lightThemeTextSchedule),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(4),
                  child: Text(
                    parseDateTimeSchedules(schedules.endTime),
                    style: TextStyle(
                      fontSize: 15,
                      color: darkMode == true
                          ? Color(Globals.darkThemeTextSchedule)
                          : Color(Globals.lightThemeTextSchedule),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
        // ),
      ),
    ]);
  }

  Widget _progressIndicator() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: Platform.isIOS
              ? CupertinoActivityIndicator(
                  radius: 20.0,
                )
              : CircularProgressIndicator(),
        ),
      ),
    );
  }

  /// Esta funcion obtiene toda la lista de programas
  Future _loadDataEvents() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      /// Parametos para la query de consulta.
      Map<String, dynamic> params = {"slug": "livetv", "platform": "movil"};

      /// Se usa la funcion del controlador para hacer la consulta mediante el uso de una instancia Dio, una query de consulta y los parametros para la query.
      await getSchedules(dio, params, Queries.getCurrentLive).then((data) {
        /// Se hace una validacion para los datos que sean nulos.
        if (data != null && data != []) {
          firstSchedule.add(data[0]);
          schedules.addAll(data);
          schedules.remove(schedules[0]);
          setState(() {
            isLoading = false;
          });
        }
      });
    }
  }

  @override
  void dispose() async {
    _connectivitySubscription.cancel();
    Screen.keepOn(false);
    super.dispose();
  }
}
