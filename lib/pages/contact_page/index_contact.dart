import 'dart:io';
import 'package:flutter/material.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexContact extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StateContact();
  }
}

class StateContact extends State<IndexContact> {
  SharedPreferences _sharedPreferences;
  bool darkMode;

  @override
  void initState() {
    // TODO: implement initState
    getDarkMode();
    super.initState();
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),
      appBar: AppBar(
        title: Text('Contacto'),
        backgroundColor: darkMode == true
            ? Color(Globals.darkThemeAppBar)
            : Color(Globals.lightThemeAppBar),
      ),
      body: contentPage(),
    );
  }

  Widget contentPage() {
    return Center(
      child: listOption(),
    );
  }

  Widget listOption() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[iconSetting(), reportProblem(), shareOpinions()],
    );
  }

  Widget reportProblem() {
    return GestureDetector(
      child: setUpCard('Soporte', Icons.build),
      onTap: () {
        String device = Platform.isIOS ? 'IOS' : 'Android';
        _openMail('Problema técnico: a través de la aplicación. dispositivo ' +
            device);
      },
    );
  }

  Widget shareOpinions() {
    return GestureDetector(
      child: setUpCard('Enviar comentarios', Icons.face),
      onTap: () {
        String device = Platform.isIOS ? 'IOS' : 'Android';
        _openMailSuggestion(
            'Háganos saber sus sugerencias: para mejorar la aplicación. dispositivo ' +
                device);
      },
    );
  }

  Widget setUpCard(String text, IconData icon) {
    return Container(
      margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: Card(
        color: darkMode == true
            ? Color(Globals.darkThemeCards)
            : Color(Globals.lightThemeCards),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            rowIcons(text, icon),
            iconCard(),
          ],
        ),
      ),
    );
  }

  Widget rowIcons(String text, IconData icon) {
    return Row(
      children: <Widget>[
        iconOptio(icon),
        titleCard(text),
      ],
    );
  }

  Widget titleCard(String text) {
    return Container(
      margin: EdgeInsets.only(left: 2.0, right: 15.0, bottom: 15.0, top: 15.0),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 17,
            color: darkMode == true
                ? Color(Globals.darkThemeText)
                : Color(Globals.lightThemeText)),
      ),
    );
  }

  Widget iconOptio(IconData icon) {
    return Container(
      margin: EdgeInsets.all(15.0),
      child: Opacity(
        opacity: 0.5,
        child: Icon(
          icon,
          size: 30,
          color: darkMode == true
              ? Color(Globals.darkThemeText)
              : Color(Globals.lightThemeText),
        ),
      ),
    );
  }

  Widget iconCard() {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Opacity(
        opacity: 0.5,
        child: Icon(
          Icons.keyboard_arrow_right,
          color: darkMode == true
              ? Color(Globals.darkThemeText)
              : Color(Globals.lightThemeText),
        ),
      ),
    );
  }

  Widget iconSetting() {
    return Container(
      child: Opacity(
        opacity: 0.5,
        child: Icon(
          Icons.contact_mail,
          size: 90,
          color: darkMode == true
              ? Color(Globals.darkThemeText)
              : Color(Globals.lightThemeText),
        ),
      ),
    );
  }

  _openMail(String option) async {
    String iosEmail = Uri.encodeComponent(option);
    if (Globals.socialEmail.length > 0) {
      if (Platform.isIOS == false) {
        try {
          await launch("mailto:" + Globals.socialEmail + "?subject=$option");
        } catch (e) {
          print(e);
        }
      } else if (Platform.isIOS) {
        try {
          await launch("mailto:" + Globals.socialEmail + "?subject=$iosEmail");
        } catch (e) {
          print(e);
        }
      }
    } else {
      print('error open email app');
    }
  }

  _openMailSuggestion(String option) async {
    String iosEmail = Uri.encodeComponent(option);
    if (Globals.socialEmail.length > 0) {
      if (Platform.isIOS == false) {
        try {
          await launch(
              "mailto:" + Globals.suggestionsEmail + "?subject=$option");
        } catch (e) {
          print(e);
        }
      } else if (Platform.isIOS) {
        try {
          await launch(
              "mailto:" + Globals.suggestionsEmail + "?subject=$iosEmail");
        } catch (e) {
          print(e);
        }
      }
    } else {
      print('error open email app');
    }
  }
}
