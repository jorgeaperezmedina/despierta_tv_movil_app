import 'dart:io';
import 'package:flutter/material.dart';
import 'package:news_app/data/globals.dart' as globals;
import 'package:news_app/widgets/menu_left/index.dart';
import 'package:launch_review/launch_review.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexMoreOptions extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StateIndexMoreOptions();
  }
}

class StateIndexMoreOptions extends State<IndexMoreOptions> {
  SharedPreferences _sharedPreferences;
  bool darkMode;
  bool active = false;
  Brightness _brightness;
  String themeUser = '';

  @override
  void initState() {
    // TODO: implement initState
    getDarkMode();
    super.initState();
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
        active = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }

      active = _sharedPreferences.getBool('isDark');
      globals.themeMode = active;
      themeUser = active == true ? 'oscuro' : 'claro';
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: darkMode == true
          ? Color(globals.darkThemeBackground)
          : Color(globals.lightThemeBackground),
      appBar: AppBar(
        backgroundColor: darkMode == true
            ? Color(globals.darkThemeAppBar)
            : Color(globals.lightThemeAppBar),
        title: Text('Más+'),
      ),

      ///Se retorna la clase con el menu lateral.
      drawer: MenuLeft(),

      /// se retorna el widget con el contenido de la vista.
      body: contentPage(),
    );
  }

  Widget contentPage() {
    ///se retorna un listview con las rutas, el icono y el nombre de las opciones.
    return ListView(
      children: <Widget>[
        card('Contáctanos', Icons.email, '/contact_page'),
        card('Puntuar aplicación', Icons.star, ''),

        ///Se hace una validacion para saber si se cuenta con alguna url para la las politicas de privacidad
        ///si se cuenta con la url se mostrara la opcion de politicas de privacidad de lo contrario se mostrara
        ///un container vacio.
        globals.urlPrivacyPolicies.isEmpty
            ? Container()
            : card('Políticas de privacidad', Icons.book, '/widget'),

        ///Esta carta se descomentmia si es necesario el uso de las notas en cache.
        // card('Ajustes', Icons.settings, '/settings_app/settings'),

        //card('Cambiar tema', Icons.phonelink_setup, '/settings_app/buttonChangeTheme')
        cardThemeMode(
            'Tema $themeUser', Icons.brightness_6, '/more_options/more_options')
      ],
    );
  }

  /// esta widget retorna un contenedor con el contenido de las opciones titulo y la navegacion a su vista correspondiente.
  Widget card(String title, IconData option, String route) {
    return Container(
      child: GestureDetector(
        child: Card(
          color: darkMode == true
              ? Color(globals.darkThemeCards)
              : Color(globals.lightThemeCards),
          child: setUpCard(title, option),
        ),
        onTap: () {
          ///Se crea una validacion para saber si es una ruta de navegacion a una vista o a la playstore.
          if (route == '') {
            _openAppOnAppOPlayStore();
          } else {
            Navigator.pushNamed(context, route);
          }
        },
      ),
    );
  }

  Widget cardThemeMode(String title, IconData option, String route) {
    return Container(
        child: Card(
      color: darkMode == true
          ? Color(globals.darkThemeCards)
          : Color(globals.lightThemeCards),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          rowIconTitle(option, title),
          Container(
            child: Switch(
              value: active == null ? false : active,
              onChanged: (value) {
                setState(() {
                  if (active == true) {
                    active = false;
                    globals.themeMode = false;
                  } else {
                    active = true;
                    globals.themeMode = true;
                  }
                  setTheme(value);
                });
              },
              activeColor: darkMode == true ? Colors.white : Colors.grey,
              inactiveThumbColor: darkMode == true ? Colors.grey : Colors.grey,
            ),
          ),
        ],
      ),
    ));
  }

  setTheme(value) async {
    _sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      active = value;
      _sharedPreferences.setBool('isDark', active);
      getDarkMode();
      print(value);
    });
  }

  Widget setUpCard(String title, IconData iconOption) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[rowIconTitle(iconOption, title), iconRow()],
    );
  }

  Widget rowIconTitle(IconData iconOption, String title) {
    return Row(
      children: <Widget>[iconCard(iconOption), titleCard(title)],
    );
  }

  Widget titleCard(String title) {
    return Container(
      margin: EdgeInsets.all(5.0),
      child: Text(
        title,
        style: TextStyle(
            fontSize: 17,
            color: darkMode == true
                ? Color(globals.darkThemeText)
                : Color(globals.lightThemeText)),
      ),
    );
  }

  Widget iconCard(IconData iconOption) {
    return Container(
      margin: EdgeInsets.all(15.0),
      child: Opacity(
        opacity: 0.5,
        child: Icon(
          iconOption,
          size: 30,
          color: darkMode == true
              ? Color(globals.darkThemeText)
              : Color(globals.lightThemeText),
        ),
      ),
    );
  }

  Widget iconRow() {
    return Container(
      child: Opacity(
        opacity: 0.5,
        child: Icon(
          Icons.keyboard_arrow_right,
          color: darkMode == true
              ? Color(globals.darkThemeText)
              : Color(globals.lightThemeText),
        ),
      ),
    );
  }

  ///Esta funcion permite el acceso a la aplicacion de la tienda dependiendo del tipo de sistema que tenga el dispositivo
  ///ya se andorid o IOS.
  _openAppOnAppOPlayStore() async {
    if (Platform.isAndroid) {
      try {
        LaunchReview.launch(androidAppId: globals.androidIdPlayStore);
      } catch (e) {
        print("No se pudo abrir flutter en el navegador.");
      }
    } else {
      try {
        LaunchReview.launch(iOSAppId: globals.iosIdAppStore);
      } catch (e) {
        print(e);
      }
    }
  }
}
