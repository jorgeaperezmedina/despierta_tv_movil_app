import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/data/mediaflix/tags_series.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:news_app/controllers/tags_controller.dart';
import 'package:dio/dio.dart';
import 'package:news_app/queries/tags.dart' as Queries;
import 'package:news_app/data/globals.dart' as Globals;
import 'package:news_app/filters.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TagSeries extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StateTagSeries();
  }
}

class StateTagSeries extends State<TagSeries> {
  Dio dio = new Dio();
  List<Tags> listTags = new List<Tags>();
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = false;
  bool serverError = false;
  bool imgError = false;
  SharedPreferences _sharedPreferences;
  bool darkMode;
  void initState() {
    ///Se inicializa la funcion _loadDataEvents para la peticion de datos.
    _loadDataEvents();
    getDarkMode();
    super.initState();
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /// se hace una validacion para la lista de tags la cual contiene los datos para la vista, en caso
    /// de que este vacia se retornara la funcion progressindicatos  de lo contrario se retornara el +
    /// contenido de la vista.
    //return _shimmerEfect() ;
    return listTags.isEmpty ? _progressIndicator() : _listTags();
  }

  ///Este widget retorna un lastview.builder el cual contiene los tags su nombre, color y las series.
  Widget _listTags() {
    return RefreshIndicator(
      onRefresh: _refreshData,
      child: Container(
        height: double.maxFinite,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: listTags.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.all(2.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ///Se hace una validacion para mostrar los valores de la lista tags en caso de que
                  ///no contenga ninguna serie se mostrara un container vacio.
                  listTags[index].series.isEmpty
                      ? Container()
                      : cardTitleTags(index),
                  listTags[index].series.isEmpty
                      ? Container()
                      : cardSliderSeriesTag(index)
                ],
              ),
            );
            // }
          },
        ),
      ),
    );
  }

  _snackBarErrorServer() {
    final snackbar = SnackBar(
      duration: const Duration(seconds: 4),
      content: Text(
        "Error al conectar con el servidor",
        style: TextStyle(fontSize: 15),
      ),
    );

    Scaffold.of(context).showSnackBar(snackbar);
  }

  ///Esta funcion cambiara los valores para la lista y se volvera hacer la peticion de datos llamando a la
  ///funcion _loadData.
  Future _refreshData() async {
    listTags = [];
    _loadDataEvents();
  }

  ///Se retorna el color del tag, su nombre y su icono.
  Widget cardTitleTags(index) {
    /// Se añade a la variable iconConvert el codigo del icono.
    // var iconConvert =
    //     listTags[index].icon == null || listTags[index].icon.isEmpty
    //         ? null
    //         : int.parse(listTags[index].icon);
    bool isUrl = false;
    var iconConvert;
    var pattern =
        r"(https?|http)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?";
    // var test = listTags[index].icon;
    //var test = "https://res.cloudinary.com/normatic/image/upload/v1579035569/Veracidad%20channel/segmentacion-Estilodevida.png";
    var match = new RegExp(pattern, caseSensitive: false)
        .allMatches(listTags[index].icon);
    match =
        RegExp(pattern, caseSensitive: false).allMatches(listTags[index].icon);
    if (match.length >= 1) {
      iconConvert = listTags[index].icon == null || listTags[index].icon.isEmpty
          ? null
          : listTags[index].icon;
      //iconConvert = test;
      isUrl = true;
    } else {
      iconConvert = listTags[index].icon == null || listTags[index].icon.isEmpty
          ? null
          : int.parse(listTags[index].icon);
      //iconConvert = int.parse(test);
      isUrl = false;
    }

    return listTags.isEmpty
        ? Container()
        : Card(
            ///se hace una validacion para a recepcion de los datos para el color del tag y se usa la funcion
            ///hexToColor para la conversion del codigo exadecimal y ser usada en el parametro color.
            color: listTags[index].color == null ||
                    listTags[index].color.isEmpty ||
                    iconConvert == null
                ? Colors.transparent
                : hexToColor(listTags[index].color),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ///Se hace una validacion para el icono en caso de que el valor que se obtenga sea nulo.
                listTags[index].icon == null ||
                        listTags[index].icon.isEmpty ||
                        iconConvert == null
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(
                            left: 14.0, top: 5.0, bottom: 5.0, right: 0.0),
                        child: isUrl == true
                            ? Container(
                                height: 40,
                                width: 40,
                                child: Image.network(iconConvert),
                              )
                            : Icon(
                                IconData(iconConvert,
                                    fontFamily: 'MaterialIcons'),
                                color: Colors.white,
                                size: 25,
                              )),
                Flexible(
                  flex: 1,
                  child: Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(
                          top: 5.0, left: 10, bottom: 5, right: 20),
                      child: SizedBox(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Text(
                            listTags[index].name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: darkMode == true
                                    ? Color(Globals.darkThemeTextTags)
                                    : Color(Globals.lightThemeTextTags)),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                      )),
                ),
              ],
            ),
          );
  }

  ///Este widget retorna todo el contenido del tag, una carta con la imagen de la serie, el titulo y sus dimenciones.
  Widget cardSliderSeriesTag(index) {
    return listTags.isEmpty
        ? Container()
        : Container(
            /// se hace una validacion para saber si el tag contiene alguna serie de lo contrario la altura se reducira a 10.
            height:
                listTags.isEmpty || listTags[index].series.isEmpty ? 10 : 290,
            child: GridView.builder(
              scrollDirection: Axis.horizontal,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 20.9 / 12.5,
                crossAxisCount: 1,
              ),
              padding: EdgeInsets.all(2.0),
              itemCount: listTags[index].series.length,
              itemBuilder: (BuildContext context, int indexSeries) {
                if (listTags.isEmpty) {
                  return Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: _progressIndicator(),
                      )
                    ],
                  );
                } else {
                  return Container(
                      width: double.infinity,
                      height: double.maxFinite,
                      child: GestureDetector(
                        ///este widget contien la imagen de la serie y el titulo.
                        child: setupCardSliderSeries(index, indexSeries),
                        onTap: () {
                          ///Esta funcion se encarga de la navegacion hacia el contenido de la serie.
                          seriesParseId(index, indexSeries);
                        },
                      ));
                }
              },
            ),
          );
  }

  Widget setupCardSliderSeries(index, indexSeries) {
    var fontSize = MediaQuery.of(context).size.height <= 1000 ? 1.1 : 1.2;
    return Card(
      elevation: 4,
      color: darkMode == true
          ? Color(Globals.darkThemeCards)
          : Color(Globals.lightThemeCards),
      child: Wrap(
        children: <Widget>[
          AspectRatio(
            aspectRatio: 8.5 / 12.5,
            child: Container(
              child: FittedBox(
                fit: BoxFit.cover,
                child: Container(
                  margin: EdgeInsets.all(1.0),
                  child:
                      _imgs(listTags[index].series[indexSeries].imgSeries[0]),
                ),
              ),
            ),
          ),
          SizedBox(
            //  width: ,
            //  height: MediaQuery.of(context).size.shortestSide/2,
            child: Container(
              width: double.maxFinite,
              margin: EdgeInsets.all(4),
              alignment: Alignment.center,
              child: Text(
                listTags[index].series[indexSeries].title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: darkMode == true
                        ? Color(Globals.darkThemeText)
                        : Color(Globals.lightThemeText)
                    // fontSize: 13
                    ),
                textScaleFactor:
                    MediaQuery.textScaleFactorOf(context) / fontSize,
                // maxLines: 1,
                // overflow: TextOverflow.ellipsis,
              ),
            ),
          )
        ],
      ),
    );
  }

  ///Esta funcion se encarga de la navegacio hacia el contenido de la serie.
  seriesParseId(index, indexSeries) {
    ///se hace una validacin para saber si la serie tiene alguna temporada, si cuanta con temporada
    ///hara una redireccion hacia la vista con el contenido, de lo contrario se mostrara una alerta
    ///indicando al usuraio que la serie no cuenta con contenido.
    if (listTags[index].series[indexSeries].seasons.isEmpty) {
      final snackbar = SnackBar(
        duration: const Duration(seconds: 4),
        content: Text(
          " La serie no cuenta con contenido ",
          style: TextStyle(fontSize: 15),
        ),
      );

      Scaffold.of(context).showSnackBar(snackbar);
    } else {
      Navigator.pushNamed(context,
          "/serie/" + listTags[index].series[indexSeries].id.toString());
    }
  }

  ///Esta funcio se encgar de la peticion de datos mediante un controlador y un modelo.
  Future _loadDataEvents() async {
    String decodeTest = "";

    ///Se hace una validacion para la variable isLoading.
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      ///parametro para la query de consulta.
      Map<String, dynamic> params = {
        "filterCategories": {
          "active": "TRUE",
          // "multimedia": {"display_type": "public"}
        },
        "filter": {"display_type": "public"},
        "seasons": {},
        "episode": {}
      };

      /// esta funcion se encarga de hacer la petiecion mediante el uso de una instancia dio,
      /// parametros para la query de consulta y una query con los datos a consultar.
      await getTags(dio, params, Queries.queryTags).then((data) {
        if (data != null) {
          listTags.addAll(data);
          setState(() {
            isLoading = false;
          });
        } else {
          setState(() {
            serverError = true;
            _snackBarErrorServer();
          });
        }
      });
    }
  }

  Widget _progressIndicator() {
    return Container(
      color: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: isLoading ? 1.0 : 00,
            child: Platform.isIOS
                ? CupertinoActivityIndicator(
                    radius: 20.0,
                  )
                : CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }

  Widget _imgs(String url) {
    if (url != null) {
      return Container(
        child: CachedNetworkImage(
          placeholder: (context, url) {
            return SpinKitThreeBounce(
              size: 30,
              color: Colors.black26,
            );
          },
          errorWidget: (context, url, error) {
            return test();
          },
          imageUrl: url,
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/not-found.jpg'), fit: BoxFit.cover)),
      );
    }
  }

  _showSnackBar(String message) {
    _scaffoldKey.currentState?.showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(seconds: 3),
    ));
  }

  ///esta funcion se encarga de retornar  un widget con una imagen en caso de que sufra
  ///algun error en la funcion img.
  test() {
    return SizedBox(
      width: 200,
      height: 200,
      child: FittedBox(
        fit: BoxFit.cover,
        child: Image.asset('assets/not-found.jpg'),
      ),
    );
  }

  @override
  void dispose() async {
    super.dispose();
  }
}
