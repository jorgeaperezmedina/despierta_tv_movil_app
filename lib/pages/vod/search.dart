import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/data/mediaflix/media.dart';
import 'package:news_app/data/mediaflix/series.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:news_app/controllers/series_controller.dart';
import 'package:dio/dio.dart';
import 'package:news_app/data/mediaflix/specials.dart';
import 'package:news_app/data/mediaflix/videos.dart';
import 'package:news_app/pages/video_player/video_test.dart';
import 'package:news_app/queries/search.dart' as Queries;
import 'package:news_app/data/globals.dart' as Globals;
import 'package:shared_preferences/shared_preferences.dart';

class IndexVodSearch extends StatefulWidget {
  final int typeContent;
  IndexVodSearch(Key key, this.typeContent) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return StateIndexVodSearch();
  }
}

class StateIndexVodSearch extends State<IndexVodSearch> {
  Series currentSeries = Series();
  bool isLoading = false;
  bool isLoadingSeries = false;
  int page = 1;
  int val = 1;
  int valLenght = 0;
  bool noFound = false;
  Dio dio = new Dio();
  List<Series> vodprograms = new List<Series>();
  List<MediaSpecial> mediaSpecials = new List<MediaSpecial>();
  String searchValue;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences _sharedPreferences;
  bool darkMode;
  List<DropdownMenuItem<int>> items = [];
  int selected;
  String typeSearch;

  void listItems() {
    items.add(new DropdownMenuItem(
      child: new Text('Programas'),
      value: 1,
    ));
    items.add(new DropdownMenuItem(
      child: new Text('Contenido'),
      value: 2,
    ));
    items.add(new DropdownMenuItem(
      child: new Text('Todos'),
      value: 3,
    ));
  }

  void initState() {
    getDarkMode();
    if (widget.typeContent == 1) {
      setState(() {
        selected = 1;
        typeSearch = 'Programas';
      });
    } else {
      setState(() {
        selected = 2;
        typeSearch = 'Contenido';
      });
    }

    super.initState();
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }
      listItems();
    });
  }

  @override
  Widget build(BuildContext context) {
    return content(context);
  }

  Widget content(BuildContext parentContext) {
    if (selected == 3) {
      setState(() {
        typeSearch = '';
      });
    }
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: darkMode == true
              ? Color(Globals.darkThemeAppBar)
              : Color(Globals.lightThemeAppBar),
          brightness: Brightness.light,
          title: TextField(
              autofocus: true,
              style: TextStyle(
                color: darkMode == true
                    ? Color(Globals.darkThemeTextTags)
                    : Color(Globals.lightThemeTextTags),
              ),
              decoration: InputDecoration(
                hintText: "Buscar $typeSearch ...",
                hintStyle: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onSubmitted: (value) {
                ////Se crea una validacion para saber si el numero de valores esta entra el rango de 1 y 2,
                ///de ser verdad se retornara la funcion _showSnackBar y se hara un refresh.
                valLenght = value.length;
                if (selected == 3) {
                  setState(() {
                    vodprograms = [];
                    mediaSpecials = [];
                  });
                }
                if (value.length == 1 || value.length == 2) {
                  _showSnackBar('Debes ingresar al menos 3 caracteres');
                  _refreshData();
                }
                if (value.isNotEmpty && value.length > 2) {
                  vodprograms = new List<Series>();
                  mediaSpecials = new List<MediaSpecial>();
                  searchValue = value;
                  if (vodprograms.isEmpty && selected == 1) {
                    _loadDataEvents();
                  } else if (mediaSpecials.isEmpty && selected == 2) {
                    _loadDataMediaSpecials();
                  } else if (vodprograms.isEmpty &&
                      mediaSpecials.isEmpty &&
                      selected == 3) {
                    _loadDataEvents();
                    // _loadDataMediaSpecials();
                  }
                }
              }),
        ),
        backgroundColor: darkMode == true
            ? Color(Globals.darkThemeBackground)
            : Color(Globals.lightThemeBackground),
        // body: textViewbody(parentContext),
        body: bodyIndex(parentContext),
      ),
    );
  }

  textViewbody(parentContext) {
    if (valLenght == 1 ||
        valLenght == 2 ||
        vodprograms.isEmpty &&
            isLoading == false &&
            valLenght > 2 &&
            selected == 1 ||
        mediaSpecials.isEmpty &&
            isLoading == false &&
            valLenght > 2 &&
            selected == 2 ||
        vodprograms.isEmpty &&
            mediaSpecials.isEmpty &&
            isLoading == false &&
            valLenght > 2 &&
            selected == 3) {
      return Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height / 2.5,
            child: Container(),
          ),
          Text(
            "Sin resultados de busqueda",
            style: TextStyle(
                fontSize: 15,
                color: darkMode == true
                    ? Color(Globals.darkThemeText).withOpacity(0.5)
                    : Color(Globals.lightThemeText).withOpacity(0.5),
                fontWeight: FontWeight.bold),
          )
        ],
      );
    } else {
      return titleSearchType();
    }
  }

  Widget bodyIndex(BuildContext parentContext) {
    return selected == 3
        ? allSections(parentContext)
        : onlyOneSearch(parentContext);
  }

  Widget onlyOneSearch(BuildContext parentContext) {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.topCenter,
          child: dropDownList(),
        ),
        textViewbody(parentContext)
      ],
    );
  }

  Widget allSections(BuildContext parentContext) {
    return ListView(
      //   mainAxisSize: MainAxisSize.max,
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        dropDownList(),
        isLoading == true
            ? Container(
                alignment: Alignment.center, child: _progressIndicator())
            : testColumn()
      ],
    );
  }

  testColumn() {
    return vodprograms.isEmpty && mediaSpecials.isEmpty
        ? Container()
        : Column(
            children: <Widget>[
              // Divider(color: darkMode == true ? Color(Globals.darkThemeText) : Color(Globals.lightThemeText),),
              Container(
                height: 30,
                color: darkMode == true
                    ? Color(Globals.darkThemeAppBar)
                    : Color(Globals.lightThemeAppBar),
                alignment: Alignment.center,
                child: Text(
                  'Programas',
                  style: TextStyle(
                      color: darkMode == true
                          ? Color(Globals.darkThemeText)
                          : Color(Globals.lightThemeTextTags),
                      fontWeight: FontWeight.bold),
                ),
              ),
              //Divider(color: darkMode == true ? Color(Globals.darkThemeText) : Color(Globals.lightThemeText),),
              vodprograms.isEmpty
                  ? Container(
                      alignment: Alignment.center,
                      height: MediaQuery.of(context).size.height / 3,
                      child: Text('Sin resultados',
                          style: TextStyle(
                              color: darkMode == true
                                  ? Color(Globals.darkThemeText)
                                  : Color(Globals.lightThemeTextSchedule))),
                    )
                  : Container(
                      alignment: Alignment.centerLeft,
                      height: MediaQuery.of(context).size.height / 3,
                      child: _listGridView(),
                    ),
              //Divider(color: darkMode == true ? Color(Globals.darkThemeText) : Color(Globals.lightThemeText),),
              Container(
                height: 30,
                color: darkMode == true
                    ? Color(Globals.darkThemeAppBar)
                    : Color(Globals.lightThemeAppBar),
                alignment: Alignment.center,
                child: Text('Contenido',
                    style: TextStyle(
                        color: darkMode == true
                            ? Color(Globals.darkThemeTextTags)
                            : Color(Globals.lightThemeTextTags),
                        fontWeight: FontWeight.bold)),
              ),
              //Divider(color:darkMode == true ? Color(Globals.darkThemeText) : Color(Globals.lightThemeText)),
              mediaSpecials.isEmpty
                  ? Container(
                      height: MediaQuery.of(context).size.height / 3,
                      alignment: Alignment.center,
                      child: Text('Sin resultados',
                          style: TextStyle(
                              color: darkMode == true
                                  ? Color(Globals.darkThemeText)
                                  : Color(Globals.lightThemeTextSchedule))),
                    )
                  : Container(
                      alignment: Alignment.centerLeft,
                      height: MediaQuery.of(context).size.height / 3,
                      child: _listGridViewMS(),
                    ),
            ],
          );
  }

  Widget dropDownList() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(5.0),
          child: Text(
            'Buscar por: ',
            style: TextStyle(
                color: darkMode == true
                    ? Color(Globals.darkThemeText)
                    : Color(Globals.lightThemeText)),
          ),
        ),
        Container(
          child: DropdownButton(
              hint: Text(
                'Seleccionar',
                style: TextStyle(
                    color: darkMode == true
                        ? Color(Globals.darkThemeText)
                        : Color(Globals.lightThemeText)),
              ),
              value: selected,
              style: TextStyle(
                color: darkMode == true
                    ? Color(Globals.lightThemeTextSchedule)
                    : Color(Globals.lightThemeText),
              ),
              items: items,
              focusColor: darkMode == true
                  ? Color(Globals.darkThemeBackground)
                  : Color(Globals.lightThemeText),
              onChanged: (value) {
                setState(() {
                  selected = value;
                  _changeView();
                  if (value == 1) {
                    setState(() {
                      typeSearch = 'Programas';
                    });
                  } else if (value == 2) {
                    setState(() {
                      typeSearch = 'Contenido';
                    });
                  } else if (value == 3) {
                    setState(() {
                      typeSearch = '';
                    });
                  }
                });
              }),
        )
      ],
    );
  }

  titleSearchType() {
    if (selected == 1) {
      return Expanded(
        child: vodprograms.isEmpty ? _progressIndicator() : _listGridView(),
      );
    } else if (selected == 2) {
      return Expanded(
        child: mediaSpecials.isEmpty ? _progressIndicator() : _listGridViewMS(),
      );
    }
  }

  Widget _listGridViewMS() {
    var aspectRation = selected == 3 ? 20.9 / 12.5 : 5.9 / 9.9;
    var numberCount = selected == 3 ? 1 : 3;
    var fontSize = MediaQuery.of(context).size.height <= 1000 ? 1.7 : 0.9;
    return mediaSpecials.contains(null)
        ? Center(child: Text("Sin resultados de busqueda"))
        : GridView.builder(
            shrinkWrap: true,
            scrollDirection: selected == 3 ? Axis.horizontal : Axis.vertical,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: aspectRation,
              crossAxisCount: numberCount,
            ),
            padding: EdgeInsets.symmetric(vertical: 5.0),
            itemCount: mediaSpecials.length,
            itemBuilder: (BuildContext parentContext, int index) {
              return GestureDetector(
                child: Card(
                  color: darkMode == true
                      ? Color(Globals.darkThemeCards)
                      : Color(Globals.lightThemeCards),
                  child: Wrap(
                    children: <Widget>[
                      AspectRatio(
                        aspectRatio: 8.5 / 12.5,
                        child: Container(
                          child: FittedBox(
                            fit: BoxFit.cover,
                            child:
                                _imgs(mediaSpecials[index].imgMediaSpecial[0]),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              child: Text(
                                mediaSpecials[index].title,
                                style: TextStyle(
                                  color: darkMode == true
                                      ? Color(Globals.darkThemeText)
                                      : Color(Globals.lightThemeText),
                                  fontWeight: FontWeight.bold,
                                  // fontSize: 11
                                ),
                                textScaleFactor:
                                    MediaQuery.textScaleFactorOf(context) /
                                        fontSize,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                onTap: () {
                  if (mediaSpecials[index].media.duration == 0 ||
                      mediaSpecials[index].media.duration == null) {
                    _showSnackBar('Error al cargar el video');
                  } else {
                    _showBottomModal(context, index);
                  }
                },
              );
            },
          );
  }

  ///Se retorna una lista con los daros de la serie, imagen, titulo y id.
  Widget _listGridView() {
    var aspectRation = selected == 3 ? 20.9 / 12.5 : 5.9 / 9.9;
    var numberCount = selected == 3 ? 1 : 3;
    var fontSize = MediaQuery.of(context).size.height <= 1000 ? 1.7 : 0.9;
    return vodprograms.contains(null)
        ? Center(child: Text("Sin resultados de busqueda"))
        : GridView.builder(
            shrinkWrap: true,
            scrollDirection: selected == 3 ? Axis.horizontal : Axis.vertical,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: aspectRation,
              crossAxisCount: numberCount,
            ),
            padding: EdgeInsets.symmetric(vertical: 5.0),
            itemCount: vodprograms.length,
            itemBuilder: (BuildContext parentContext, int index) {
              return GestureDetector(
                child: Card(
                  color: darkMode == true
                      ? Color(Globals.darkThemeCards)
                      : Color(Globals.lightThemeCards),
                  child: Wrap(
                    children: <Widget>[
                      AspectRatio(
                          aspectRatio: 8.5 / 12.5,
                          child: Container(
                            child: FittedBox(
                              fit: BoxFit.cover,
                              child: _imgs(vodprograms[index].imgSeries[0]),
                            ),
                          )),
                      Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              child: Text(
                                vodprograms[index].title,
                                style: TextStyle(
                                  color: darkMode == true
                                      ? Color(Globals.darkThemeText)
                                      : Color(Globals.lightThemeText),
                                  fontWeight: FontWeight.bold,
                                  // fontSize: 11
                                ),
                                textScaleFactor:
                                    MediaQuery.textScaleFactorOf(context) /
                                        fontSize,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.pushNamed(
                      context, "/serie/" + vodprograms[index].id.toString(),
                      arguments: vodprograms);
                },
              );
            },
          );
  }

  Future _showBottomModal(BuildContext context, int index) async {
    // await generateOption(video);

    print(mediaSpecials[index].media.videos[0].urlVideo);
    await showModalBottomSheet(
        context: context,
        builder: (BuildContext _context) {
          return Container(
            // color: ,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: _generateOptions(
                    context, mediaSpecials[index].media.videos, index),
              ),
            ),
            decoration: BoxDecoration(
                color: Theme.of(_context).canvasColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
          );
        });
  }

  List<Widget> _generateOptions(
      BuildContext context, List<Videos> videos, int index) {
    List<Widget> listTiles = new List<Widget>();
    listTiles.add(Stack(
      children: <Widget>[
        Container(
          color: darkMode == true
              ? Color(Globals.darkThemeAppBar)
              : Color(Globals.lightThemeAppBar),
          width: double.infinity,
          height: 56.0,
          child: Center(
            child: Text(
              "Calidad",
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
          ),
        )
      ],
    ));
    if (videos.length == 0 || videos.length == null) {
      listTiles.add(ListTile(
        title: Text('Sin calidad de video disponible'),
        onTap: () {
          Navigator.of(context).pop();
        },
      ));
    }
    for (var i = 0; i < videos.length; i++) {
      if (videos[i].height != null || videos[i].height != 0) {
        listTiles.add(Container(
            color: darkMode == true
                ? Color(Globals.darkThemeBackground)
                : Color(Globals.lightThemeBackground),
            child: ListTile(
              // leading: Icon(Icons.hd), + "(${videos[i].height.toString()})"
              title: Row(
                children: <Widget>[
                  Text(
                    videos[i].videoType.toUpperCase() + " ",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: darkMode == true
                            ? Color(Globals.darkThemeText)
                            : Color(Globals.lightThemeText)),
                  ),
                  Text(
                    "(${videos[i].height.toString()})",
                    style: TextStyle(
                        color: darkMode == true
                            ? Color(Globals.darkThemeText)
                            : Color(Globals.lightThemeText)),
                  )
                ],
              ),
              onTap: () {
                Navigator.of(context).pop();
                _playVideo(context, videos[i].urlVideo, [], index);
              },
            )));
      }
    }
    return listTiles;
  }

  void _playVideo(
      BuildContext context, String url, List<dynamic> video, int index) {
    String titlevideo = mediaSpecials[index].title;
    Duration duration = Duration(seconds: mediaSpecials[index].media.duration);
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration: const Duration(milliseconds: 1000),
        pageBuilder: (BuildContext context, _, __) {
          return new VideoWidget(url, false, duration, titlevideo, []);
          // return new VideoPlayer(url, duration);
        }));
  }

  Widget _progressIndicator() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: Platform.isIOS
              ? CupertinoActivityIndicator(
                  radius: 20.0,
                )
              : CircularProgressIndicator(),
        ),
      ),
    );
  }

  ///esta funcion  se encarga de la peticion de los datos mediante un controlador y un modelo.
  Future _loadDataEvents() async {
    ///se hace una validacion a la varible isLoading.
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      ///mapa con los parametros para query de consulta.
      Map<String, dynamic> params = {
        "filter": {
          "search": searchValue,
          //  "active": "TRUE"
        },
        "season": {"display_type": "public"},
        "episode": {"display_type": "public"},
        "serie": {"display_type": "public"}
      };

      ///Se usa la funcion getSeries con una instanca de dio, parametros para para la query y una query con los datos de consulta.
      // await getSeries(dio, params, Queries.search).then((data) {
      await getSeries(dio, params, Queries.search).then((data) {
        if (data != null) {
          vodprograms.addAll(data);
          print(vodprograms);
          setState(() {
            noFound = false;
            isLoading = false;
          });
        }
      });
      if (mediaSpecials.isEmpty && selected == 3) {
        _loadDataMediaSpecials();
      }
    }
  }

  Future _loadDataMediaSpecials() async {
    ///se hace una validacion a la varible isLoading.
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      ///mapa con los parametros para Fquery de consulta.
      Map<String, dynamic> params = {
        "filter": {"search": searchValue}
      };

      ///Se usa la funcion getSeries con una instanca de dio, parametros para para la query y una query con los datos de consulta.
      // await getMediaSpecials(dio, params, QueriesMedia.searchMediaSpecial)
      //     .then((data) {
      //   print(data);
      //   if (data != null) {
      //     mediaSpecials.addAll(data);
      //     setState(() {
      //       noFound = false;
      //       isLoading = false;
      //     });
      //   }
      // });
    }
  }

  _showSnackBar(String message) {
    return _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(fontSize: 15),
      ),
    ));
  }

  Future _refreshData() async {
    page = 1;
    setState(() {
      vodprograms = [];
      mediaSpecials = [];
      _loadDataEvents();
      _loadDataMediaSpecials();
    });
  }

  Future _changeView() async {
    page = 1;
    setState(() {
      vodprograms = [];
      mediaSpecials = [];

      if (searchValue.isNotEmpty && selected == 1) {
        _loadDataEvents();
      } else if (searchValue.isNotEmpty && selected == 2) {
        _loadDataMediaSpecials();
      } else if (searchValue.isNotEmpty && selected == 3) {
        _loadDataEvents();
      }
    });
  }

  Widget _imgs(String url) {
    if (url != null) {
      return Container(
        child: CachedNetworkImage(
          placeholder: (context, url) {
            return SpinKitThreeBounce(
              size: 30,
              color: Colors.black26,
            );
          },
          errorWidget: (context, url, error) {
            return Icon(Icons.error);
          },
          imageUrl: url,
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/not-found.jpg'), fit: BoxFit.cover)),
      );
    }
  }

  @override
  void dispose() async {
    super.dispose();
  }
}
