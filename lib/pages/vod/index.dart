import 'package:flutter/material.dart';
import 'package:news_app/pages/vod/search.dart';
import 'package:news_app/widgets/menu_left/index.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:news_app/pages/vod/tags_series.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexVod extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StateIndexVod();
  }
}

class StateIndexVod extends State<IndexVod> {
  SharedPreferences _sharedPreferences;
  bool darkMode;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDarkMode();
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }
      Globals.themeMode = darkMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Programas',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: darkMode == true
            ? Color(Globals.darkThemeAppBar)
            : Color(Globals.lightThemeAppBar),
        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.only(right: 20),
            icon: Icon(
              Icons.search,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {
              ///El navigator te enviara a la clase del buscador.
              // Navigator.pushNamed(context, '/vod_vc/vod');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          IndexVodSearch(null, 1)));
            },
          )
        ],
      ),
      backgroundColor: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),

      ///Esta es el nombre de la clase para las opciones del menu lateral.
      drawer: MenuLeft(),
      body:

          ///Esta es la clase que contiene todos los widgets de la vista de seies.
          TagSeries(),
    );
  }
}
