import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:news_app/data/mediaflix/series.dart';
import 'package:news_app/widgets/menu_left/index.dart';
import 'package:news_app/widgets/videos/sliver_app_bar.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:news_app/controllers/series_controller.dart';
import 'package:dio/dio.dart';
import 'package:news_app/queries/episode.dart' as Queries;
import 'package:news_app/widgets/series/episode_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShowEpisode extends StatefulWidget {
  final String episodeId;
  final String seasonId;
  final String serieId;

  ShowEpisode({Key key, this.episodeId, this.serieId, this.seasonId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return StateShowEpisode();
  }
}

class StateShowEpisode extends State<ShowEpisode> {
  Series serie = new Series();
  ScrollController _scrollController = new ScrollController();
  GlobalKey<ScaffoldState> _scaffoldKey;
  bool isLoading = false;
  bool lastStatus = true;
  Dio dio = new Dio();
  SharedPreferences _sharedPreferences;
  bool darkMode;

  _scrollListener() {
    if (isShrink != lastStatus) {
      setState(() {
        lastStatus = isShrink;
      });
    }
  }

  bool get isShrink {
    return _scrollController.hasClients &&
        _scrollController.offset >
            (Globals.heightSelectedDrawer - kToolbarHeight);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          Globals.pageViewedNow = '/vod_vc';
          Navigator.pushReplacementNamed(context, '/vod_vc');
          return Future.value(false);
        },
        child: Scaffold(
          drawer: MenuLeft(),
          key: _scaffoldKey,
          body: LayoutBuilder(
            builder: (context, constraints) {
              if (serie.title == null) {
                return _progressIndicator();
              } else {
                return _content();
              }
            },
          ),
          resizeToAvoidBottomPadding: false,
        ));
  }

  @override
  void initState() {
    getDarkMode();
    _loadData();
    super.initState();
    _scaffoldKey = GlobalKey();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {}
    });
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
        Globals.themeMode = darkMode;
      }
    });
  }

  Widget _content() {
    String _img = serie.imgSeries != null ? serie.imgSeries[0] : "";
    return Container(
      color: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),
      //height: double.maxFinite,
      child: CustomScrollView(controller: _scrollController, slivers: <Widget>[
        SliverAppCard(serie.title, true, _img),
        // buttomSeasons(false),
        // _buildList()
        stickyHeaderSeasonButtom()
      ]),
    );
  }

  Future _loadData() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      Map<String, dynamic> params = {
        "id": widget.serieId,
        "season": {"id": widget.seasonId, "display_type": "public"},
        "episode": {"id": widget.episodeId, "display_type": "public"}
      };
      await getSerie(dio, params, Queries.queryEpisode).then((data) {
        if (data != null) {
          setState(() {
            isLoading = false;
            serie = data;
          });
        } else {
          setState(() {
            isLoading = false;
          });
        }
      });
    }
  }

  Widget stickyHeaderSeasonButtom() {
    return SliverStickyHeaderBuilder(
      builder: (context, state) => new Container(
        height: 45.0,
        color: darkMode == true
            ? Colors.grey[800].withOpacity(1.0 - state.scrollPercentage)
            : Colors.grey[900].withOpacity(1.0 - state.scrollPercentage),
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        alignment: Alignment.centerLeft,
        child: Container(
          child: buttomSeasons(state),
        ),
      ),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext parentcontext, int index) {
            if (serie.title == null || serie.seasons.length == 0) {
              return _progressIndicator();
            } else {
              return _validSerieHasEpisodes(serie);
            }
          },
          childCount: 1,
        ),
      ),
    );
  }

  Widget buttomSeasons(state) {
    return Container(
      child: FlatButton(
        textColor: darkMode == true
            ? Color(Globals.darkThemeTextButtonSeason)
            : Color(Globals.lightThemeTextButtonSeason),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Text(
                serie.seasons[0].title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Text(''),
          ],
        ),
        onPressed: () {},
      ),
    );
  }

  Widget _progressIndicator() {
    return Container(
      color: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: isLoading ? 1.0 : 0,
            // opacity: 1.0,
            child: Platform.isIOS
                ? CupertinoActivityIndicator(
                    radius: 20.0,
                  )
                : CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }

  Widget _validSerieHasEpisodes(serie) {
    if (serie.seasons.length > 0) {
      if (serie.seasons[0].episodes.length > 0) {
        return EpisodeCard(serie.seasons[0].episodes[0]);
      } else {
        return _notFound();
      }
    } else {
      return _notFound();
    }
  }

  Widget _buildList() {
    return ListView.builder(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      physics: BouncingScrollPhysics(),
      // controller: _scrollController,
      itemCount: 1,
      itemBuilder: (BuildContext context, int index) {
        if (serie.title == null) {
          return _progressIndicator();
        } else {
          return _validSerieHasEpisodes(serie);
        }
      },
    );
  }

  Widget _notFound() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: Card(
        elevation: 8,
        child: Column(
          children: <Widget>[
            Container(
              child: FittedBox(
                child: SizedBox(
                    height: 350,
                    width: 455,
                    child: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/image-not-found.png'))),
                    )),
              ),
            ),
            Text(
              "Contenido no disponible",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.5),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
    super.dispose();
  }
}
