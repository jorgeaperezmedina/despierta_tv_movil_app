import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:news_app/data/mediaflix/videos.dart';
import 'package:news_app/widgets/menu_left/index.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:expandable/expandable.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:news_app/controllers/series_controller.dart';
import 'package:dio/dio.dart';
import 'package:news_app/data/mediaflix/series.dart';
import 'package:news_app/queries/series.dart' as Queries;
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:news_app/widgets/series/episode_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:async';

class ShowSerie extends StatefulWidget {
  final int id;
  final int seasonId;
  final bool openByNotification;
  ShowSerie({Key key, this.id, this.seasonId, this.openByNotification})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return StateShow();
  }
}

class StateShow extends State<ShowSerie> with SingleTickerProviderStateMixin {
  bool isLoading = false;
  int page = 1;
  Dio dio = new Dio();
  Series _serie = new Series();
  GlobalKey<ScaffoldState> _scaffoldKey;
  int indexSeason = 0;
  bool hasVideo;
  ScrollController _scrollController = new ScrollController();
  SharedPreferences _sharedPreferences;

  bool lastStatus = true;

  double div = 0;
  bool darkMode;

  ///Esta funcion valida si la variable isShrink es diferente a la variable lastStatus.
  _scrollListener() {
    if (isShrink != lastStatus) {
      setState(() {
        lastStatus = isShrink;
      });
    }
  }

  ///Se obtiene un valor booleano si el controlador _scrollController es menor que la resta de la altura del contenedor del sliver appbar.
  bool get isShrink {
    return _scrollController.hasClients &&
        _scrollController.offset >
            (((MediaQuery.of(context).size.aspectRatio *
                        MediaQuery.of(context).size.shortestSide) /
                    div) -
                kToolbarHeight);
  }

  @override
  void initState() {
    ///Se establece un controlador para el widget Scrollable.
    _scrollController = ScrollController();

    ///Se añade un listener al controlador para saber los cambios de los valores de los pixeles.
    _scrollController.addListener(_scrollListener);

    ///Se inicializa la  funcion para obtencion de datos.
    _loadData();
    getDarkMode();
    super.initState();

    ///Se inicializa una etiqueta global para la depuracion.
    _scaffoldKey = GlobalKey();
    // _scrollController.addListener(() {
    //   if (_scrollController.position.pixels ==
    //       _scrollController.position.maxScrollExtent) {
    //     _loadData();
    //   }
    // });
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }

      Globals.themeMode = darkMode;
    });
  }

  @override
  void dispose() {
    ///se elimina el listener del controlador _scrollController.
    _scrollController.removeListener(_scrollListener);

    /// Se elimina el controlador _scrollController.
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies

    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(ShowSerie oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (!widget.openByNotification) {
          Navigator.pop(context, false);
          return Future.value(false);
        }
        {
          Globals.pageViewedNow = '/vod_vc';
          Navigator.pushReplacementNamed(context, '/vod_vc');
          // Navigator.pop(context, false);
          return Future.value(false);
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: darkMode == true
            ? Color(Globals.darkThemeBackground)
            : Color(Globals.lightThemeBackground),

        ///Se hace una validacion para las notificaciones, se elimina el menu lateral.
        drawer: widget.openByNotification ? MenuLeft() : null,
        body: RefreshIndicator(

            ///Se usa la funcion _refreshData, para cualquier nuevo cambio en la vista.
            onRefresh: _refreshData,
            child: LayoutBuilder(
              builder: (context, constraints) {
                ///Se valida si hay algun valor nulo en la vista.
                if (_serie.title == null) {
                  ///Se retorna la funcion _progressIndicator en caso de haber un valor nulo.
                  return _progressIndicator();
                } else {
                  ///Se retorna todo el contenido de la vista si no se obtiene ningun valor nulo.
                  return seriesContent();
                }
              },
            )),
        resizeToAvoidBottomPadding: false,
      ),
    );
  }

  ///Esta funcion realiza la peticion de datos mediante el uso del controlador en la carpeta controller.
  Future _loadData() async {
    ///Se hace una validacion si la varible isLoading esta en true.
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      ///Se usa la funcion getSerie del controlador en la carpeta controll, para la peticion de datos.
      ///con parametros como son una instancia dio, parametros para la query y una query de consulta con estructura de graphql.
      await getSerie(
              dio,
              {
                'id': widget.id.toString(),
                'page': page,
                "SeasonFilter": {"display_type": "public"},
                "orderSeason": "DESC"
              },
              Queries.findSerie)
          .then((data) {
        ///Se hace una validacion para la recepcion de datos.
        if (data != null) {
          setState(() {
            isLoading = false;
            _serie = data;
            page += 1;
          });
          try {
            if (widget.seasonId != 0 && widget.seasonId != null) {
              int index = _serie.seasons
                  .indexWhere((season) => season.id == widget.seasonId);
              indexSeason = index;
            }
          } catch (e) {
            print(e);
            print(
                "ocurrio un error al asignar la temporada de la notificacion");
          }
        } else {
          setState(() {
            isLoading = false;
          });
          _showSnackBar("No hay mas videos por cargar");
        }
      });
    }
  }

  ///Este widget contiene un widget customScrollView para la recepcion de la funcion sliverAPPBar y la descripcion.
  Widget seriesContent() {
    return Container(
        color: darkMode == true
            ? Color(Globals.darkThemeBackground)
            : Color(Globals.lightThemeBackground),
        child: CustomScrollView(
          controller: _scrollController,
          slivers: <Widget>[
            ///Esta funcion contiene un widget SliverAppBAr.
            sliverAppBar(),

            ///Esta funcion contiene un widget SliverGrit.
            _serie.longDescription == null
                ? SliverGrid.count(
                    crossAxisCount: 1,
                    children: <Widget>[],
                  )
                : sliverGridTextDescription(),

            ///Esta funcion contiene un widget stickyHeader.
            stickyHeaderSeasonButtom()
          ],
        ));
  }

  ///Este widget contiene un sliver appbar para la vista de series.

  Widget sliverAppBar() {
    var div = MediaQuery.of(context).size.height <= 1000 ? 1.1 : 1.25;

    return SliverAppBar(
      centerTitle: true,
      backgroundColor: darkMode == true
          ? Color(Globals.darkThemeAppBar)
          : Color(Globals.lightThemeAppBar),
      pinned: true,
      floating: false,
      snap: false,

      ///Se añade una altura para la cracion del sliverAppBar este valor es tomado del archivo globals.
      expandedHeight: (MediaQuery.of(context).size.aspectRatio *
              MediaQuery.of(context).size.shortestSide) /
          div,

      //Globals.heightSliverAppbar,
      flexibleSpace: FlexibleSpaceBar(
          // collapseMode: CollapseMode.parallax,
          centerTitle: true,
          title: ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: (MediaQuery.of(context).size.aspectRatio *
                        MediaQuery.of(context).size.shortestSide) /
                    div),
            child: Text(
              _serie.title,

              ///Se hace una validacion para un tamaño customizado para el titulo en caso de que sea demaciado extenso.
              textScaleFactor: isShrink == true ? 1 : 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            ),
          ),
          background: Container(
            child: Container(
              width: double.infinity,
              child: GestureDetector(
                child: Container(
                  color: Colors.black,
                  // color: Color(Globals.color),
                  //color: darkMode == true ? null : Color(Globals.colorAppBar),
                  child: Opacity(
                    opacity: 0.7,
                    // child: FittedBox(
                    //   fit: BoxFit.cover,
                    //   child: Image.asset("assets/test1.jpg"),
                    // )
                    child: _imgs(
                        _serie.seasons[indexSeason].imgSeason[indexSeason]),
                  ),
                ),
                onTap: () {},
              ),
            ),
          )),
    );
  }

  ///Se utiliza un widget sliverGrid para la descripcion de la serie ya que con este widget fue posible agregar
  ///la descripcion al sliverAppBar.
  Widget sliverGridTextDescription() {
    var divs = _serie.longDescription.length <= 300 ? 1.4 : 2.5;
    return SliverGrid.count(
      childAspectRatio: (MediaQuery.of(context).size.aspectRatio *
              MediaQuery.of(context).size.longestSide) /
          (_serie.longDescription.length / divs),
      crossAxisCount: 1,
      children: <Widget>[
        Container(
          color: darkMode == true
              ? Color(Globals.darkThemeBackground).withOpacity(0.8)
              : Color(Globals.lightThemeBackground).withOpacity(0.9),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: textDescription(),
          ),
        )
      ],
    );
  }

  ///Este widget retorna un widget SliverStickyHeaderBuilder el cual proporciona una funcion y efecto necesario
  ///para el boton de temporadas.
  Widget stickyHeaderSeasonButtom() {
    return SliverStickyHeaderBuilder(
      builder: (context, state) => new Container(
        height: 45.0,

        ///se establece un color para la barra.
        color: darkMode == true
            ? Colors.grey[800].withOpacity(1.0 - state.scrollPercentage)
            : Colors.grey[900].withOpacity(1.0 - state.scrollPercentage),
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        alignment: Alignment.centerLeft,
        child: Container(
          child: buttomSeasons(state),
        ),
      ),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext parentcontext, int index) {
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                child:
                    EpisodeCard(_serie.seasons[indexSeason].episodes[index]));
          },
          childCount: _serie.seasons[indexSeason].episodes.length,
        ),
      ),
    );
  }

  Widget textDescription() {
    double fontSizes = MediaQuery.of(context).size.height <= 1000 ? 1.1 : 1.6;
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(10),
      child: Text(
        _serie.longDescription,
        style: TextStyle(
            color: darkMode == true
                ? Color(Globals.darkThemeText)
                : Color(Globals.lightThemeText)),
        textScaleFactor: fontSizes,
      ),
    );
  }

  Widget setupAlertDialog() {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: _serie.seasons.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: FlatButton(
              child: Text(
                _serie.seasons[index].title,
                style: TextStyle(
                    color: darkMode == true
                        ? Color(Globals.darkThemeTextTags)
                        : Color(Globals.lightThemeTextTags),
                    fontSize: 20),
              ),
              onPressed: () {
                if (indexSeason == index) {
                  Navigator.of(context).pop();
                } else {
                  indexSeason = index;
                  _refreshData();
                  Navigator.of(context).pop();
                }
              },
            ),
          );
        },
      ),
    );
  }

  Widget episodeNumbre(index) {
    return Text("Ep." +
        _serie.seasons[indexSeason].episodes[index].numberEpisode.toString());
  }

  Widget seasonNumber() {
    return Center(
      child: Text(
        _serie.seasons[indexSeason].title,
        style: TextStyle(
            color: darkMode == true
                ? Color(Globals.darkThemeText)
                : Color(Globals.lightThemeText),
            fontSize: 20),
      ),
    );
  }

  Widget _progressIndicator() {
    return Container(
      color: darkMode == true
          ? Color(Globals.darkThemeBackground)
          : Color(Globals.lightThemeBackground),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: isLoading ? 1.0 : 0,
            // opacity: 1.0,
            child: Platform.isIOS
                ? CupertinoActivityIndicator(
                    radius: 20.0,
                  )
                : CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }

  ///Esta funcion asincrona cambia los valores de las variables page, _serie  y se vuelve a llamar a la funcion
  ///_loadData para hacer de nuevo la peticion de datos.
  Future _refreshData() async {
    page = 1;
    _serie = new Series();
    _loadData();
  }

  ///En este widget se hace una validacion para las urls de las imagenes y se utiliza el widget cachenetworkimage para
  ///las imagenes y haciendo otra validacion en caso de encontrar un error al momento de cargar las imagenes ya se que
  ///no se encuantren en la url proporcionada.
  Widget _imgs(String url) {
    if (url != null) {
      return Container(
        child: CachedNetworkImage(
          placeholder: (context, url) {
            return SpinKitThreeBounce(
              size: 30,
              color: Colors.black,
            );
          },
          errorWidget: (context, url, error) {
            return Icon(Icons.error);
          },
          imageUrl: url,
          fit: BoxFit.cover,
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/not-found.jpg'), fit: BoxFit.cover)),
      );
    }
  }

  _showSnackBar(String message) {
    _scaffoldKey.currentState?.showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(seconds: 3),
    ));
  }

  Widget buttomSeasons(state) {
    return Container(
      child: FlatButton(
        textColor: darkMode == true
            ? Color(Globals.darkThemeTextButtonSeason)
            : Color(Globals.lightThemeTextButtonSeason),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Text(
                _serie.seasons[indexSeason].title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: state.isPinned ? 15 : 20),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),

            ///Se hace una validacion si la serie cuenta con mas de una temporada se mostrara un icono arrow_drop_down en la barra de tempordas y
            ///se podra acceder a la funcion alertDialog de lo contrario no semostrara el icono arrow_drop_down y no se podra acceder a la funcion alertDialog.
            _serie.seasons.length > 1
                ? IconButton(
                    disabledColor: Colors.white,
                    icon: Icon(Icons.arrow_drop_down),
                    onPressed: () {
                      alertDialog();
                    },
                  )
                : Text(''),
          ],
        ),
        onPressed: () {
          alertDialog();
        },
      ),
    );
  }

  ///Esa funcion retorna una alerta que le muesra al usuario el numero de temporadas que cuenta la serie.
  alertDialog() {
    ///Se hace una validacion para el numero de temporadas que cuenta la serie, en caso de que sea mayor a 1,
    ///se mostrara en forma de lista las temporadas y se accedera al contenido de ellas, de lo contrario el alert dialog
    ///retornara un valor nulo.
    if (_serie.seasons.length > 1) {
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Colors.transparent.withOpacity(0.5),
          content: setupAlertDialog(),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Cerrar',
                style: TextStyle(color: Colors.red, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
    } else {
      return null;
    }
  }
}
