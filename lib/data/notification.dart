import 'dart:async';

class NotificationItem{
  String title;
  String noteId;
  String episodeId;
  String serieId;
  String seasonId;
  bool liveStream;
  NotificationItem({this.title, this.noteId, this.liveStream, this.episodeId, this.serieId, this.seasonId});
  StreamController<NotificationItem> _controller = StreamController<NotificationItem>.broadcast();
  Stream<NotificationItem> get onChanged => _controller.stream;
  
  
  factory NotificationItem.forMessage(Map<String, dynamic> data){
    return NotificationItem(
      title: data["title"] ?? '',
      //noteId: int.parse(data["note"]) ?? 0,
      noteId: data["note"] ?? '',
      serieId: data["serieId"] ?? '',
      liveStream: data["liveStream"] ?? false,
      episodeId: data["episodeId"] ?? '',
      seasonId: data["seasonId"] ?? ''
    );
  }
}