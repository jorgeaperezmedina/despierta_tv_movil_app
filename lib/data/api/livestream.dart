class LiveStream {
  String url;
  LiveStream({this.url});

  factory LiveStream.fromC10(Map<String, dynamic> data){
    return LiveStream(url: data['url']);
  }
  
}
LiveStream validateLiveStream(json, typeApi){
  switch (typeApi) {
    case "apic10":
      return LiveStream.fromC10(json);
      break;
    default:
      return LiveStream.fromC10(json);
  }
}