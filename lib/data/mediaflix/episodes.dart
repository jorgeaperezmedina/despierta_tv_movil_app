import 'package:news_app/data/mediaflix/media.dart';

class Episodes {
  int id, numberEpisode;
  String title, longDescription, shortDescription, releaseDate;
  List<String> imgEpisodes;
  Media media;

  Episodes({
    this.id,
    this.title,
    this.longDescription,
    this.shortDescription,
    this.releaseDate,
    this.numberEpisode,
    this.imgEpisodes,
    this.media,
  });

  factory Episodes.fromgraphql(Map<String, dynamic> data) {
    List<String> listImgEpisode = [];
    for (var i = 0; i < data['thumbnail'].length; i++) {
      listImgEpisode.add(data['thumbnail']);
    }
    return Episodes(
        id: data['id'],
        title: data['title'],
        longDescription: data['longDescription'],
        shortDescription: data['shortDescription'],
        releaseDate: data['releaseDate'],
        numberEpisode: data['numberEpisode'],
        imgEpisodes: listImgEpisode,
        media: Media.formGraphql(data['media']));
  }

  factory Episodes.fromTvStream(Map<String, dynamic> data) {
    return Episodes();
  }
}

Episodes validateEpisodes(json, typeApi) {
  switch (typeApi) {
    case 'test':
      return Episodes.fromTvStream(json);
      break;
    default:
      return Episodes.fromgraphql(json);
      break;
  }
}
