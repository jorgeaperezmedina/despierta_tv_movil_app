import 'package:news_app/data/mediaflix/seasons.dart';

class Series {
  int id;
  String title, longDescription, shortDescription, releaseDate;
  List<String> imgSeries;
  List<Seasons> seasons;

  Series({
    this.id,
    this.title,
    this.longDescription,
    this.shortDescription,
    this.releaseDate,
    this.imgSeries,
    this.seasons,
  });

  factory Series.fromgraphql(Map<String, dynamic> data) {
    List<String> listImgSeries = [];
    for (var i = 0; i < data['thumbnail'].length; i++) {
      listImgSeries.add(data['thumbnail']);
    }
    return Series(
      id: data['id'],
      title: data['title'],
      longDescription: data['longDescription'],
      shortDescription: data['shortDescription'],
      releaseDate: data['releaseDate'],
      imgSeries: listImgSeries,
      seasons: parseSeasons(data['seasons']),
    );
  }
  factory Series.fromTvStream(Map<String, dynamic> data) {
    return Series();
  }

  static List<Seasons> parseSeasons(seasonsjson) {
    try {
      var list = seasonsjson as List;
      List<Seasons> listSeasons =
          list.map((data) => Seasons.fromGraphql(data)).toList();
      return listSeasons;
    } catch (e) {
      return null;
    }
  }
}

Series validateSeries(json, typeApi) {
  switch (typeApi) {
    case 'test':
      return Series.fromTvStream(json);
      break;
    default:
      return Series.fromgraphql(json);
      break;
  }
}
