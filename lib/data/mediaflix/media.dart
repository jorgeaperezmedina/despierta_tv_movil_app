import 'package:news_app/data/mediaflix/videos.dart';

class Media {
  String name;
  int duration;
  List<Videos> videos;

  Media({
    this.name,
    this.duration,
    this.videos,
  });

  factory Media.formGraphql(Map<String, dynamic> data) {
    return Media(
      name: data['name'],
      duration: data['duration'],
      videos: parseVideos(data['videos']),
    );
  }

  factory Media.formTvEstream(Map<String, dynamic> data) {
    return Media();
  }

  static List<Videos> parseVideos(parsejson) {
    var list = parsejson as List;
    List<Videos> listVideos =
        list.map((data) => Videos.fromgraphql(data)).toList();
    return listVideos;
  }
}

Media validateMedia(json, typeApi) {
  switch (typeApi) {
    case 'test':
      return Media.formTvEstream(json);
      break;
    default:
      return Media.formGraphql(json);
      break;
  }
}
