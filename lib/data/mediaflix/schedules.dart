class Schedules {
  int id;
  String title;
  String shortDescription;
  String endTime;
  int duration;
  String beginTime;
  List<dynamic> currentLives;
  List<dynamic> liveEvents;

  Schedules(
      {this.id,
      this.title,
      this.shortDescription,
      this.endTime,
      this.duration,
      this.beginTime,
      this.currentLives,
      this.liveEvents});

  factory Schedules.fromgraphql(Map<String, dynamic> data) {
    return Schedules(
        id: data['id'],
        title: data['title'],
        shortDescription: data['shortDescription'],
        endTime: data['endTime'],
        duration: data['duration'],
        beginTime: data['beginTime']);
  }

  factory Schedules.fromTvStream(Map<String, dynamic> data) {
    return Schedules();
  }
}

Schedules validateSchedules(json, typeApi) {
  switch (typeApi) {
    case 'test':
      return Schedules.fromTvStream(json);
      break;
    default:
      return Schedules.fromgraphql(json);
      break;
  }
}
