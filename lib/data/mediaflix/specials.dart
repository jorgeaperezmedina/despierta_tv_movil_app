import 'package:news_app/data/mediaflix/media.dart';


class MediaSpecial{

  String  title, longDescription, displayType, releaseDate;
  List<String> imgMediaSpecial;
  String id;
  Media media;

  MediaSpecial({
    this.id,
    this.title,
    this.longDescription,
    this.imgMediaSpecial,
    this.displayType,
    this.releaseDate,
    this.media
  });

  factory MediaSpecial.fromgraphql(Map <String, dynamic> data){

    List<String> listImage = [];
    for (var i = 0; i < data['thumbnail'].length; i++) {
      listImage.add(data['thumbnail']);
    }
    return MediaSpecial(
          id: data['id'],
          title: data['title'],
          longDescription: data['longDescription'],
          imgMediaSpecial: listImage,
          displayType: data['displayType'],
          releaseDate: data['releaseDate'],
          media: Media.formGraphql(data['media'])
    );
  }

  factory MediaSpecial.fromTvStream(Map<String, dynamic> data){
    return MediaSpecial();
  }
}
  MediaSpecial validateMediaSpecial(json, typeApi){
    switch (typeApi) {
      case 'apic10':
        return MediaSpecial.fromTvStream(json);
        break;
      default:
      return MediaSpecial.fromgraphql(json);
      break;
    }
  }