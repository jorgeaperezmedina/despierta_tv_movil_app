import 'package:news_app/data/mediaflix/series.dart';
import 'package:news_app/data/mediaflix/specials.dart';

class Tags {
  int id;
  String name;
  String color;
  String icon;
  List<Series> series;
  List<MediaSpecial> mediaSpecial;

  Tags(
      {this.id,
      this.name,
      this.color,
      this.series,
      this.mediaSpecial,
      this.icon});

  factory Tags.fromGraphql(Map<String, dynamic> data) {
    return Tags(
        id: data['id'],
        name: data['name'],
        color: data['color'],
        series: parseSeries(data['series']),
        icon: data['icon'],
        mediaSpecial: parseMediaSpecial(data['mediaSpecials']));
  }

  factory Tags.fromTvStream(Map<String, dynamic> data) {
    return Tags();
  }

  static List<Series> parseSeries(seasonsjson) {
    try {
      var list = seasonsjson as List;
      List<Series> listSeasons =
          list.map((data) => Series.fromgraphql(data)).toList();
      return listSeasons;
    } catch (e) {
      return null;
    }
  }

  static List<MediaSpecial> parseMediaSpecial(mediaSpecialjson) {
    try {
      var list = mediaSpecialjson as List;
      List<MediaSpecial> listMediaSpecial =
          list.map((data) => MediaSpecial.fromgraphql(data)).toList();
      return listMediaSpecial;
    } catch (e) {
      return [];
    }
  }
}

Tags validateTags(json, typeApi) {
  switch (typeApi) {
    case 'apic10':
      return Tags.fromTvStream(json);
      break;
    default:
      return Tags.fromGraphql(json);
      break;
  }
}
