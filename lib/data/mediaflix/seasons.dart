import 'package:news_app/data/mediaflix/episodes.dart';

class Seasons {
  int id, seasonNumber;
  String title;
  List<String> imgSeason;
  List<Episodes> episodes;

  Seasons(
      {this.id, this.title, this.seasonNumber, this.imgSeason, this.episodes});

  factory Seasons.fromGraphql(Map<String, dynamic> data) {
    List<String> listImgSeason = [];
    for (var i = 0; i < data['thumbnail'].length; i++) {
      listImgSeason.add(data['thumbnail']);
    }

    return Seasons(
      id: data['id'],
      title: data['title'],
      seasonNumber: data['seasonNumber'],
      episodes: parseEpisodes(data['episodes']),
      imgSeason: listImgSeason,
    );
  }

  factory Seasons.fromTvStream(Map<String, dynamic> data) {
    return Seasons();
  }

  static List<Episodes> parseEpisodes(episodesjson) {
    try {
      var list = episodesjson as List;
      List<Episodes> listEpisodes =
          list.map((data) => Episodes.fromgraphql(data)).toList();
      return listEpisodes;
    } catch (e) {
      return null;
    }
  }
}

Seasons validateSeason(json, typeApi) {
  switch (typeApi) {
    case 'test':
      return Seasons.fromTvStream(json);
      break;
    default:
      return Seasons.fromGraphql(json);
      break;
  }
}
