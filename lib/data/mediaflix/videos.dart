class Videos {
  String urlVideo, id;
  int height;
  String videoType;

  Videos({this.id, this.urlVideo, this.videoType, this.height});

  factory Videos.fromgraphql(Map<String, dynamic> data) {
    return Videos(
        id: data['id'],
        urlVideo: data['url'],
        videoType: data['quality'],
        height: data['height']);
  }

  factory Videos.fromTvStream(Map<String, dynamic> dta) {
    return Videos();
  }
}

Videos validateVideos(json, typeApi) {
  switch (typeApi) {
    case 'test':
      return Videos.fromTvStream(json);
      break;
    default:
      return Videos.fromgraphql(json);
      break;
  }
}
