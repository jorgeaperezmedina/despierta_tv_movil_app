import 'package:dio/dio.dart';
import 'package:news_app/data/mediaflix/specials.dart';
import 'package:news_app/services/config-client.dart' as configClient;
import 'package:news_app/data/mediaflix/series.dart';
import 'package:news_app/services/api-client.dart' as apiClient;

Future getSeries(
    Dio dio, Map<String, dynamic> queryParams, String query) async {
  try {
    Response response = await apiClient.postGraphqlDio(dio, queryParams, query);
    if (response.statusCode == 200) {
      return parseSeries(response.data["data"]["series"]);
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

Future getSerie(Dio dio, Map<String, dynamic> queryParams, String query) async {
  try {
    Response response = await apiClient.postGraphqlDio(dio, queryParams, query);
    if (response.statusCode == 200) {
      print(response.data);
     
        return parseSerie(response.data["data"]["serie"]);
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

List<Series> parseSeries(List<dynamic> json) {
  return json.map((data) => validateSeries(data, configClient.tenant)).toList();
}

Series parseSerie(dynamic json) {
  return validateSeries(json, configClient.tenant);
}

