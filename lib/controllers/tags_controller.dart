import 'package:dio/dio.dart';
import 'package:news_app/services/config-client.dart' as configClient;
import 'package:news_app/services/api-client.dart' as apiClient;
import 'package:news_app/data/mediaflix/tags_series.dart';

Future getTags(Dio dio, Map<String, dynamic> queryParams, String query) async {
  try {
    Response response = await apiClient.postGraphqlDio(dio, queryParams, query);
    if (response.statusCode == 200) {
      return parseTags(response.data["data"]["fetchCategories"]);
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

List<Tags> parseTags(List<dynamic> json) {
  return json.map((data) => validateTags(data, configClient.tenant)).toList();
}
