import 'package:dio/dio.dart';
import '../data/mediaflix/schedules.dart';
import 'package:news_app/services/config-client.dart' as configClient;
import 'package:news_app/services/api-client.dart' as apiClient;

Future getSchedules(
    Dio dio, Map<String, dynamic> queryParams, String query) async {
  try {
    Response response = await apiClient.postGraphqlDio(dio, queryParams, query);
    if (response.statusCode == 200) {
      return parseSchedule(response.data["data"]["currentLives"]["liveEvents"]);
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

List<Schedules> parseSchedule(List<dynamic> json) {
  return json
      .map((data) => validateSchedules(data, configClient.tenant))
      .toList();
}
