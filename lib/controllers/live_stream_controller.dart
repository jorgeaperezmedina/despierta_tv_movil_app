import 'package:news_app/services/api-client.dart' as apiClient;
import 'package:news_app/services/config-client.dart' as configClient;
import 'package:dio/dio.dart';
import 'package:news_app/data/api/livestream.dart';

Future getLiveStreamGraphql(
    Dio _dio, Map<String, dynamic> queryParams, String query) async {
  try {
    Response response =
        await apiClient.postGraphqlDio(_dio, queryParams, query);
    if (response.statusCode == 200) {
      return parseLiveStream(
          response.data["data"]["currentLives"]["liveUrls"][0]);
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

LiveStream parseLiveStream(dynamic json) {
  return validateLiveStream(json, configClient.tenant);
}
