import 'package:flutter/material.dart';
import 'package:news_app/pages/vod/show_serie.dart';
import 'package:news_app/data/globals.dart' as globals;
import 'package:flutter/rendering.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:news_app/libraries.dart';
import 'package:news_app/pages/vod/index.dart';
import 'package:news_app/pages/contact_page/index_contact.dart';
import 'package:news_app/pages/vod/episode/show.dart';
import 'package:news_app/data/notification.dart';
import 'dart:io';
import 'package:plain_notification_token/plain_notification_token.dart';
import 'dart:convert';
import 'package:news_app/pages/more_options/more_options.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:news_app/pages/liveStreamDespiertaTV/liveStreamDespiertTV.dart';

typedef Future<dynamic> MessageHandler(Map<String, dynamic> message);
PlainNotificationToken _plainNotificationToken;
BuildContext parentContext;

void main() async {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      brightness: Brightness.light,
      primarySwatch: Colors.red,
      buttonColor: Colors.blueAccent,
    ),
    home: MyApp(),
    routes: routes(BuildContext),
    onGenerateRoute: generateRoute,
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    _plainNotificationToken = PlainNotificationToken();
    initConfigParse();
    setPortraitOrientation();
  }

  @override
  Widget build(BuildContext context) {
    parentContext = context;
    return MaterialApp(
      title: globals.title,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        buttonColor: Colors.blueAccent,
      ),
      home: globals.viewHome,
      routes: routes(context),
      onGenerateRoute: generateRoute,
    );
  }

  void initConfigParse() {
    _plainNotificationToken.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage example: $message");
        if (Platform.isAndroid) {
          Map<String, dynamic> newMap = jsonDecode(message['data']);
          _navigateToItemDetail(newMap);
        }
        if (Platform.isIOS) {
          print("no funciona como deberia la push notification");
          // _navigateToItemDetail(message);
        }
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch example: $message");
        if (Platform.isAndroid) {
          Map<String, dynamic> newMap = jsonDecode(message['data']);
          _navigateToItemDetail(newMap);
        }
        if (Platform.isIOS) {
          _navigateToItemDetail(message);
        }
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume example: $message");
        if (Platform.isAndroid) {
          Map<String, dynamic> newMap = jsonDecode(message['data']);
          _navigateToItemDetail(newMap);
        }
        if (Platform.isIOS) {
          _navigateToItemDetail(message);
        }
      },
    );
    _plainNotificationToken.autoInitParse(
        globals.parseServerUrl, globals.parseAppId, globals.parseClientKey);
  }

  Future<void> _navigateToItemDetail(Map<String, dynamic> message) async {
    try {
      NotificationItem notification = NotificationItem.forMessage(message);

      if (notification.episodeId != '' && notification.episodeId != null) {
        print("navegara al episodio");
        String route = '/show_episode/' +
            notification.serieId +
            '/' +
            notification.seasonId +
            '/' +
            notification.episodeId;
        print("route" + route);
        globals.pageViewedNow = 'show_episode';
        await Navigator.pushReplacementNamed(context, route);
      } else if (notification.serieId != '' && notification.serieId != null) {
        print("navegara al serie & season");
        String route = '/serie/' +
            notification.serieId +
            '/season/' +
            notification.seasonId +
            '/notification';
        print("route" + route);
        globals.pageViewedNow = route;
        await Navigator.pushReplacementNamed(context, route);
      }
    } catch (e) {
      print("ocurrio un erro al momento del parseo de la notificacion");
      print(e);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}

Map<String, WidgetBuilder> routes(context) {
  return {
    '/vod': (context) => IndexVod(),
    '/contact_page': (context) => IndexContact(),
    '/more_options/more_options': (context) => IndexMoreOptions(),
    '/liveStreamDespiertaTV': (context) => IndexLiveStreamDT(),
    "/widget": (_) => new WebviewScaffold(
          url: globals.urlPrivacyPolicies,
          appBar: new AppBar(
            backgroundColor: globals.themeMode == true
                ? Color(globals.darkThemeAppBar)
                : Color(globals.lightThemeAppBar),
            title: new Text("Politicas de privacidad"),
          ),
          withJavascript: true,
          withLocalStorage: true,
          withZoom: true,
          hidden: true,
        ),
  };
}

Route<dynamic> generateRoute(settings) {
  final List<String> pathElements = settings.name.split('/');
  if (pathElements[0] != '') {
    return null;
  }
  if (pathElements[1] == 'serie') {
    final int id = int.parse(pathElements[2]);
    int seasonId = 0;
    bool openByNotification = false;
    try {
      openByNotification = pathElements[5] == "notification" ? true : false;
      seasonId = int.parse(pathElements[4]);
    } catch (e) {
      print("no se pasa el season id");
    }
    return MaterialPageRoute<bool>(
        builder: (context) => ShowSerie(
            id: id,
            seasonId: seasonId,
            openByNotification: openByNotification));
  }
  if (pathElements[1] == 'show_episode') {
    final int serieId = int.parse(pathElements[2]);
    final int seasonId = int.parse(pathElements[3]);
    final int episodeId = int.parse(pathElements[4]);

    return MaterialPageRoute<bool>(
      builder: (context) => ShowEpisode(
          serieId: serieId.toString(),
          seasonId: seasonId.toString(),
          episodeId: episodeId.toString()),
    );
  }
  return null;
}
