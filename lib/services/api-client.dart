import 'dart:async';
import 'package:http/http.dart' as http;
import './config-client.dart' as configClient;
import 'package:dio/dio.dart';

Future<Response> postGraphqlDio(
    Dio dio, Map<String, dynamic> queryParams, String query) async {
  try {
    Response response = await dio.post(configClient.baseUrl,
        options: Options(headers: {
          'Content-Type': 'application/json',
          'current-tenant': configClient.tenantEncoded
        }),
        queryParameters: {'query': query, 'variables': queryParams});
    return response;
  } catch (e) {
    print("error de dio graphql->");
    print(e);
    throw Exception('Falied load data');
  }
}
