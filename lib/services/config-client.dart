import 'package:news_app/data/globals.dart' as globals;

String baseUrl = globals.urlBase;
String tenant = globals.tenant;
String tenantEncoded = globals.tenantEncode;
String formateDates = "yyyy-MM-ddTHH:mm:ss";
