import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:news_app/data/mediaflix/episodes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:news_app/filters.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:news_app/data/mediaflix/videos.dart';
import 'package:news_app/pages/video_player/video_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EpisodeCard extends StatelessWidget {
  final Episodes episode;
  //final int index;
  EpisodeCard(this.episode);

  SharedPreferences sharedPreferences;
  bool darkMode;

  getDarkMode() async {
    sharedPreferences = await SharedPreferences.getInstance();

    if (sharedPreferences.getBool('isDark') == null) {
      darkMode = false;
    } else {
      darkMode = sharedPreferences.getBool('isDark');
    }
  }

  @override
  Widget build(BuildContext context) {
    return _episodeCard(context);
  }

  Widget _episodeCard(BuildContext context) {
    String titleEpisode = "Ep.${episode.numberEpisode} " + episode.title;
    //  print(titleEpisode.length);

    int div = MediaQuery.of(context).size.height <= 1000 ? 5 : 12;
    return Padding(
        padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
        child: GestureDetector(
          child: Card(
              color: Globals.themeMode == true
                  ? Color(Globals.darkThemeCards)
                  : Color(Globals.lightThemeCards),
              elevation: 8,
              child: Column(
                children: <Widget>[
                  Container(
                    child: FittedBox(
                      child: SizedBox(
                        height: (MediaQuery.of(context).size.aspectRatio *
                                MediaQuery.of(context).size.shortestSide) /
                            0.52,
                        width: (MediaQuery.of(context).size.aspectRatio *
                                MediaQuery.of(context).size.shortestSide) /
                            0.4,
                        //height: 350,
                        //width: 455,
                        child: imgIconPlay(
                            true,
                            // widget.series.seasons[numberSeason].episodes[index]
                            //     .imgEpisodes[index],
                            episode.imgEpisodes[0],
                            context),
                      ),
                    ),
                  ),
                  ExpandablePanel(
                    controller: ExpandableController(initialExpanded: true),
                    header: Container(
                      width: double.maxFinite,
                      margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.only(
                                  left: 10,
                                  bottom: episode.longDescription == null ||
                                          episode.longDescription.isEmpty
                                      ? 15
                                      : 0),
                              height: (MediaQuery.of(context).size.aspectRatio *
                                      MediaQuery.of(context)
                                          .size
                                          .shortestSide) /
                                  div,
                              child: Text(
                                titleEpisode,
                                maxLines: titleEpisode.length < 20 ? 1 : 2,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Globals.themeMode == true
                                        ? Color(Globals.darkThemeText)
                                        : Color(Globals.lightThemeText),
                                    fontSize: 16.5),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    expanded: Column(
                      children: <Widget>[
                        episode.longDescription == null
                            ? Container()
                            : Container(
                                margin: EdgeInsets.only(
                                    left: 20, bottom: 10, right: 20, top: 5),
                                child: Text(
                                  // widget.series.seasons[numberSeason].episodes[index]
                                  //     .longDescription
                                  episode.longDescription,
                                  softWrap: true,
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Globals.themeMode == true
                                          ? Color(Globals.darkThemeText)
                                          : Color(Globals.lightThemeText)),
                                ))
                      ],
                    ),
                    tapBodyToCollapse: false,
                    tapHeaderToExpand: false,
                    hasIcon: false,
                    iconPlacement: ExpandablePanelIconPlacement.right,
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                  ),
                ],
              )),
          onTap: () {
            _showBottomModal(context);
          },
        ));
  }

  Widget imgIconPlay(bool iconPlay, String imgUrl, context) {
    Duration duration = Duration(seconds: episode.media.duration);
    var sizesheight = MediaQuery.of(context).size.height <= 1000 ? 3.8 : 5.5;
    var sizeswidth = MediaQuery.of(context).size.height <= 1000 ? 2 : 2.8;

    String url;
    if (iconPlay) {
      return Container(
          // height: 200,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: CachedNetworkImageProvider(imgUrl),
            ),
          ),
          child: Stack(
            children: <Widget>[
              Positioned(
                right: 20.0,
                bottom: 20.0,
                child: Container(
                  alignment: Alignment.center,
                  height: (MediaQuery.of(context).size.aspectRatio *
                          MediaQuery.of(context).size.shortestSide) /
                      sizesheight,
                  width: (MediaQuery.of(context).size.aspectRatio *
                          MediaQuery.of(context).size.shortestSide) /
                      sizeswidth,
                  //height: 35.0,
                  //width: 100.5,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black87),
                  child:
                      // widget.series.seasons[numberSeason].episodes[index]
                      //         .media.duration
                      episode.media.duration == null
                          ? Icon(
                              Icons.play_arrow,
                              size: 30.0,
                              color: Colors.white,
                            )
                          : Container(
                              margin: EdgeInsets.all(5),
                              child: parsetimerDuration(duration.inMinutes,
                                  context, duration.inSeconds),
                            ),
                  // child: Icon(Icons.play_circle_filled, color: Colors.white,),
                ),
              ),
            ],
          ));
    } else {
      return Container(
        child: CachedNetworkImage(
          placeholder: (context, url) => SpinKitThreeBounce(
            color: Colors.black26,
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
          imageUrl: url,
        ),
      );
    }
  }

  Future _showBottomModal(BuildContext context) async {
    await showModalBottomSheet(
        context: context,
        builder: (BuildContext _context) {
          return Container(
            // color: ,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: _generateOptions(context, episode.media.videos),
              ),
            ),
            decoration: BoxDecoration(
                color: Theme.of(_context).canvasColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
          );
        });
  }

  List<Widget> _generateOptions(BuildContext context, List<Videos> videos) {
    List<Widget> listTiles = new List<Widget>();
    listTiles.add(Stack(
      children: <Widget>[
        Container(
          color: Globals.themeMode == true
              ? Color(Globals.darkThemeAppBar)
              : Color(Globals.lightThemeAppBar),
          width: double.infinity,
          height: 56.0,
          child: Center(
            child: Text(
              "Calidad",
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
          ),
        )
      ],
    ));
    if (videos.length == 0 || videos.length == null) {
      listTiles.add(ListTile(
        title: Text('Sin calidad de video disponible'),
        onTap: () {
          Navigator.of(context).pop();
        },
      ));
    }
    for (var i = 0; i < videos.length; i++) {
      listTiles.add(Container(
          color: Globals.themeMode == true
              ? Color(Globals.darkThemeBackground)
              : Color(Globals.lightThemeBackground),
          child: ListTile(
            // leading: Icon(Icons.hd), + "(${videos[i].height.toString()})"
            title: Row(
              children: <Widget>[
                Text(
                  videos[i].videoType.toUpperCase() + " ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Globals.themeMode == true
                          ? Color(Globals.darkThemeText)
                          : Color(Globals.lightThemeText)),
                ),
                Text(
                  "(${videos[i].height.toString()})",
                  style: TextStyle(
                      color: Globals.themeMode == true
                          ? Color(Globals.darkThemeText)
                          : Color(Globals.lightThemeText)),
                )
              ],
            ),
            onTap: () {
              Navigator.of(context).pop();
              _playVideo(context, videos[i].urlVideo, episode.media.videos);
            },
          )));
    }
    return listTiles;
  }

  void _playVideo(BuildContext context, String url,
      List<dynamic> video /* or list of urls */) {
    String titlevideo = "Ep.${episode.numberEpisode}" + ' ' + episode.title;
    Duration duration = Duration(seconds: episode.media.duration);
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration: const Duration(milliseconds: 1000),
        pageBuilder: (BuildContext context, _, __) {
          return new VideoWidget(
              url, false, duration, titlevideo, episode.media.videos);
        }));
  }
}
