import 'package:flutter/material.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SliverAppCard extends StatelessWidget{
  
  final String title;
  final bool isShrink;
  final String imgUrl;
  //final String description;
  //Widget widgetButton;

  SliverAppCard(this.title,this.isShrink,this.imgUrl);

  @override
  Widget build(BuildContext context) {
    // List<Widget> listWidgets = new List<Widget>();
    // listWidgets.addAll([
    //     sliverAppBar(title,isShrink,imgUrl),
    //     sliverGridTextDescription(description),
    //     widgetButton
    //   ]
    // );
    return sliverAppBar(title,isShrink,imgUrl);
  }

  Widget sliverAppBar(String title, bool isShrink, String imgUrl) {
    return SliverAppBar(
      centerTitle: true,
      backgroundColor: Color(Globals.color),
      pinned: true,
      floating: false,
      snap: false,
      expandedHeight: Globals.heightSelectedDrawer,
      flexibleSpace: FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          centerTitle: true,
          title: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 250),
            child: Text(
              isShrink == true ? title : "",
              textScaleFactor: isShrink == true ? 1 : 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            ),
          ),
          background: Container(
            child: Opacity(
                opacity: 1,
                child: Container(
                  width: double.infinity,
                  child: GestureDetector(
                    child: Container(
                      color: Colors.black,
                      child: Opacity(
                        opacity: 0.7,
                        child: _imgs(imgUrl),
                      ),
                    ),
                    onTap: () {},
                  ),
                )),
          )),
    );
  }

  Widget _imgs(String url) {
    if (url != null) {
      return Container(
        child: CachedNetworkImage(
          placeholder: (context, url) {
            return SpinKitThreeBounce(
              size: 30,
              color: Colors.black,
            );
          },
          errorWidget: (context, url, error) {
            return Icon(Icons.error);
          },
          imageUrl: url,
          fit: BoxFit.cover,
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/not-found.jpg'), fit: BoxFit.cover)),
      );
    }
  }
  
}