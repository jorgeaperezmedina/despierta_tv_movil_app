import 'package:flutter/material.dart';
import 'package:news_app/widgets/menu_left/options.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:news_app/widgets/menu_left/social_networks.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuLeft extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MenuState();
  }
}

class MenuState extends State<MenuLeft> {
  SharedPreferences _sharedPreferences;
  bool darkMode;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  getDarkMode() async {
    _sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      if (_sharedPreferences.getBool('isDark') == null) {
        darkMode = false;
      } else {
        darkMode = _sharedPreferences.getBool('isDark');
      }

      Globals.themeMode = darkMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(canvasColor: Colors.black12
          // canvasColor: Theme.of(context).hintColor
          ),
      child: Drawer(
        child: _container(context),
      ),
    );
  }

  Widget _container(context) {
    return Column(
      children: <Widget>[
        DrawerHeader(
            margin: EdgeInsets.symmetric(vertical: 0.0),
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: _imgUserTop(context),
                  )
                ],
              ),
            )),
        Divider(color: Colors.white),
        Expanded(child: ListView(children: optionsMenu(context))),
        Divider(color: Colors.white),
        Text(
          'Contáctanos por',
          style: TextStyle(color: Colors.white),
        ),
        SingleChildScrollView(
          child: listSocialNetworks(),
          scrollDirection: Axis.horizontal,
        )
      ],
    );
  }

  Widget _imgUserTop(context) {
    return Semantics(
        explicitChildNodes: true,
        child: Image.asset(Globals.darkThemeDrawerLogo));
  }

  List<Widget> optionsMenu(context) {
    return [liveStreamDT(context), series(context), contactPage(context)];
  }

  Widget listSocialNetworks() {
    return Container(
      child: Wrap(
        spacing: -6,
        children: <Widget>[
          facebook(context),
          twitter(context),
          instagram(context)
        ],
      ),
    );
  }
}
