import 'package:flutter/material.dart';
import 'package:news_app/data/globals.dart' as Globals;
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences _sharedPreferences;
bool darkMode;

getDarkMode() async {
  _sharedPreferences = await SharedPreferences.getInstance();
  if (_sharedPreferences.getBool('isDark') == null) {
    darkMode = false;
  } else {
    darkMode = _sharedPreferences.getBool('isDark');
  }
}

colorMode() {
  if (Globals.themeMode == false || Globals.themeMode == null) {
    return Color(Globals.colorAppBar);
  } else {
    return Color(Globals.darkThemeAppBar);
  }
}

Widget series(context) {
  return Container(
    color: Globals.pageViewedNow == '/vod' ? colorMode() : Colors.transparent,
    child: ListTile(
      leading: Icon(
        Icons.local_play,
        color: Colors.white,
      ),
      title: Text(
        "Programas",
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      onTap: () {
        if (Globals.pageViewedNow == '/vod') {
          Navigator.of(context).pop();
        } else {
          Globals.pageViewedNow = '/vod';
          Navigator.pushReplacementNamed(context, '/vod');
        }
      },
    ),
  );
}

Widget liveStreamDT(context) {
  return Container(
    color: Globals.pageViewedNow == '/liveStreamDespiertaTV'
        ? colorMode()
        : Colors.transparent,
    child: ListTile(
      leading: Icon(
        Icons.live_tv,
        color: Colors.white,
      ),
      title: Text(
        "Ver TV en Vivo",
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      onTap: () {
        if (Globals.pageViewedNow == '/liveStreamDespiertaTV') {
          Navigator.of(context).pop();
        } else {
          Globals.pageViewedNow = '/liveStreamDespiertaTV';
          Navigator.pushReplacementNamed(context, '/liveStreamDespiertaTV');
        }
      },
    ),
  );
}

Widget contactPage(context) {
  return Container(
    color: Globals.pageViewedNow == '/more_options/more_options'
        ? colorMode()
        : Colors.transparent,
    child: ListTile(
      leading: Icon(
        Icons.more_horiz,
        color: Colors.white,
      ),
      title: Text(
        "Más+",
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      onTap: () {
        if (Globals.pageViewedNow == '/more_options/more_options') {
          Navigator.of(context).pop();
        } else {
          Globals.pageViewedNow = '/more_options/more_options';
          Navigator.pushReplacementNamed(context, '/more_options/more_options');
        }
      },
    ),
  );
}
