import 'dart:io';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:news_app/data/globals.dart' as globals;
// import 'package:news_app/data/globals.dart' as globals;

Widget facebook(context) {
  return GestureDetector(
    onTap: () {
      _openFb(context, 'Facebook');
    },
    child: buttonSocial("assets/social/whites/fb.png"),
  );
}

Widget twitter(context) {
  return GestureDetector(
    onTap: () {
      _openTwitter(context, 'Twitter');
    },
    child: buttonSocial("assets/social/whites/tw.png"),
  );
}

Widget linkedin() {
  return GestureDetector(
    onTap: () {
      _openLinkedin();
    },
    child: buttonSocial("assets/social/whites/lk.png"),
  );
}

Widget youtube() {
  return GestureDetector(
    onTap: () {},
    child: buttonSocial("assets/social/whites/yt.png"),
  );
}

Widget instagram(context) {
  return GestureDetector(
    onTap: () {
      _openInstagram(context, 'Instagram');
    },
    child: buttonSocial("assets/social/whites/int.png"),
  );
}

Widget flickr() {
  return GestureDetector(
    onTap: () {
      _openFlickr();
    },
    child: buttonSocial("assets/social/whites/fc.png"),
  );
}

Widget mail() {
  return GestureDetector(
    onTap: () {
      _openMail();
    },
    child: buttonSocial("assets/social/whites/mail.png"),
  );
}

Widget whatsapp(context) {
  return GestureDetector(
    onTap: () {
      _openWhatsapp(context, 'Whatsapp');
    },
    child: buttonSocial("assets/social/whites/wt.png"),
  );
}

Widget movilNumber() {
  return GestureDetector(
    onTap: () {
      _openTel();
    },
    child: buttonSocial("assets/social/whites/call.png"),
  );
}

Widget buttonSocial(String imgAsset) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 12, horizontal: 15),
    height: 30,
    width: 30,
    decoration: BoxDecoration(
        image: DecorationImage(
      image: AssetImage(imgAsset),
    )),
  );
}

_openFb(context, String nameAPP) async {
  try {
    await canLaunch(globals.socialIDFB)
        ? launch(globals.socialIDFB)
        : launch(globals.socialLinkFB);
  } catch (e) {
    _modalBottomSheet(context, nameAPP);
  }
}

_openWhatsapp(context, String nameApp) async {
  var whatsappUrl = "whatsapp://send?phone=" + globals.socialLinkWhatsApp;
  await launch(whatsappUrl);
}

_openTwitter(context, String nameAPP) async {
  var linkTW = globals.socialIDTW;
  try {
    await canLaunch(linkTW) ? launch(linkTW) : launch(globals.socialLinkTW);
  } catch (e) {
    _modalBottomSheet(context, nameAPP);
  }
}

_openMail() async {
  if (globals.socialEmail.length > 0) {
    try {
      await launch("mailto:" +
          globals.socialEmail +
          "?subject=Info&body=A%20quien%20corresponda");
    } catch (e) {
      print("No se pudo abrir el correo.");
    }
  } else {}
}

_openLinkedin() async {
  if (Platform.isAndroid) {
    try {
      await launch(globals.socialLinkLinkedin);
    } catch (e) {
      print("No se pudo abrir flutter en el navegador.");
    }
  } else {
    await launch(globals.socialLinkLinkedin);
  }
}

_openInstagram(context, String nameAPP) async {
  var instagram = globals.socialIDInstagram;
  try {
    await canLaunch(instagram)
        ? launch(instagram)
        : launch(globals.socialLinkInstagram);
  } catch (e) {
    _modalBottomSheet(context, nameAPP);
  }
}

_openFlickr() async {
  try {
    await launch(globals.socialLinkFlirck);
  } catch (e) {
    print("No se pudo abrir flutter en el navegador.");
  }
}

_openTel() async {
  var numberPhone = "tel://" + globals.socialPhone;
  await canLaunch(numberPhone) ? launch(numberPhone) : print("");
}

_modalBottomSheet(context, String nameAPP) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          height: 50,
          alignment: Alignment.center,
          color: Colors.grey[800],
          child: new Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              new ListTile(
                  // leading: new Icon(Icons.signal_wifi_off),
                  title: new Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(3.0),
                        child: Icon(
                          Icons.error,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        'Error: verifica tu Aplicación ' +
                            ' ' +
                            "\"" +
                            nameAPP +
                            "\"",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onTap: () => {}),
            ],
          ),
        );
      });
}
