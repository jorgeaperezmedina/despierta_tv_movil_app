String getCurrentLive = r'''
query getLive($slug: String,$platform: String){
            currentLives(slug: $slug, platform: $platform){
                    id
                    title
                    status
                    liveEvents {
                        id
                        title
                        day
                        beginTime
                        endTime
                        duration
                        urlImage
                        shortDescription
                    }
                }
        }
''';