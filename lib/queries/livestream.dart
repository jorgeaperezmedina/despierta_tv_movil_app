String getLive = r'''
query getLive($slug: String,$platform: String){
    currentLives(slug: $slug, platform: $platform){
      id
      title
      status
      liveUrls{
        url
      }
    }
}
''';