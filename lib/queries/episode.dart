
String queryEpisode = r'''
query($id: ID, $season: SeasonFilter, $episode: EpisodeFilter){
  serie(id: $id){
      id
      shortDescription
      title
      thumbnail
      releaseDate
      longDescription
    seasons(filter: $season){
        id
        title
        seasonNumber
        thumbnail
      episodes(filter: $episode){
        longDescription
          shortDescription
          thumbnail
          id
          media{
            name
            duration
            videos{
              url
              quality
              id
              videoType
              bitRate
              height
              width 
            }
          }
          title
          releaseDate
          numberEpisode
        
      }
    }
  }
}

''';