String search= r'''
query($filter: SerieFilter, $season: SeasonFilter, $episode: EpisodeFilter){
  series(filter: $filter){
     id
        title
        thumbnail
        releaseDate
        seasons(filter: $season) {
          id
          thumbnail
          episodes(filter: $episode) {
            id
            media{
              name
              duration
              videos{
                url
                quality
                id
                videoType
                bitRate
                height
                width 
              }
            }
          }
        }
    
  }
}''';

String onlySeries = r'''
  query ($filter: SerieFilter, $season: SeasonFilter, $episode: EpisodeFilter) {
   series(filter: $filter){
     id
        title
        thumbnail
        seasons(filter: $season) {
          id
          title
          episodes(filter: $episode) {
            id
            title
          }
        }
  }

''';


String serieAndMediaSpecials = r'''
query ($filter: SerieFilter, $season: SeasonFilter, $episode: EpisodeFilter) {
   series(filter: $filter){
     id
        shortDescription
        title
        thumbnail
        releaseDate
        longDescription
        seasons(filter: $season) {
          id
          title
          seasonNumber
          thumbnail
          episodes(filter: $episode) {
            longDescription
            shortDescription
            thumbnail
            id
            media{
              name
              duration
              videos{
                url
                quality
                id
                videoType
                bitRate
                height
                width 
              }
            }
            title
            releaseDate
            numberEpisode
          }
        }
  },
     mediaSpecials(filter: $filter) {
    id
    title
    longDescription
    thumbnail
    displayType
    releaseDate
    media {
      duration
      name
      videos {
        id
        url
        videoType
        bitRate
        quality
        height
        width
      }
      images {
        url
        quality
      }
    }
  }
}

''';