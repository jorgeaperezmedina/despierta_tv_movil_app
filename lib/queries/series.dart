String series = '''
query {
  series{
    id
    shortDescription
    title
    thumbnail
    releaseDate
    longDescription
    tags {id name}
    seasons {
      id
      title
      seasonNumber
      thumbnail
      episodes {
        longDescription
        shortDescription
        thumbnail
        id
        media{
          name
          duration
          videos{
            url
            quality
            id
            videoType
            bitRate
            height
            width 
          }
        }
        title
        releaseDate
        numberEpisode
      }
    }
  }
}

''';

String findSerie = r'''
query($id: ID, $orderSeason: SortOrder, $SeasonFilter: SeasonFilter){
  serie(id: $id){
    id
    shortDescription
    title
    thumbnail
    releaseDate
    longDescription
    tags {id name}
    seasons(filter: $SeasonFilter, order: $orderSeason) {
      id
      title
      seasonNumber
      thumbnail
      episodes {
        longDescription
        shortDescription
        thumbnail
        id
        media{
          name
          duration
          videos{
            url
            quality
            id
            videoType
            bitRate
            height
            width 
          }
        }
        title
        releaseDate
        numberEpisode
      }
    }
  }
}

''';