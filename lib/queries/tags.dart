
// String queryTags = r'''
// query($filterCategori: CategoryFilter,$filter: SerieFilter){
// 	 fetch_categories(filter: $filterCategori){
//     id
//     name
//     icon
//     color
//     series(filter: $filter){
//     id
//         shortDescription
//         title
//         thumbnail
//         releaseDate
//         longDescription
      
//       seasons{
//         	id
//           title
//           seasonNumber
//           thumbnail
        
//         episodes{
          
//           longDescription
//             shortDescription
//             thumbnail
//             id
//             media{
//               name
//               duration
//               videos{
//                 url
//                 quality
//                 id
//                 videoType
//                 bitRate
//                 height
//                 width 
//               }
//             }
//             title
//             releaseDate
//             numberEpisode
          
//         }
//       }
//     }
//   }
// }
// ''';
String queryTags = r'''
query($filterCategories: CategoryFilter, $filter: SerieFilter,$season: SeasonFilter, $episode: EpisodeFilter){
	fetchCategories(filter: $filterCategories){
    id
    name
    icon
    color
    series(filter: $filter){
     	 	id
        title
        thumbnail        
      seasons(filter:$season){
        	id
        thumbnail
        episodes(filter:$episode){
          id
        }
      }
    }
  }
}

''';

String findMediaSpecial =  r'''
query($id: ID){
      mediaSpecial(id: $id){
        id,
        title,
        longDescription,
        thumbnail
        displayType
        releaseDate,
        media {
          videos {
            id,
            url,
            videoType,
            bitRate,
            quality
          },
          images{
            url,
            quality
          }
        }
    tags{
      id,
      name,
    }
      }
}
''';