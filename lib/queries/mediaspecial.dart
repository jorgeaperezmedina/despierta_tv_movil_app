String queryMediaSpecial = r'''
    query($filterCategori: CategoryFilter, $filter: MediaSpecialFilter){
	fetchCategories(filter: $filterCategori){
    id
    name
    icon
    color
    mediaSpecials(filter: $filter){
     	 id
      title,
        thumbnail
        media {
          duration,
          name
          videos {
            id,
            url,
            videoType,
            bitRate,
            quality,
            height,
            width
          },
          images{
            url,
            quality
          }
        }
      
    }
  }
}


''';
String onlySpecials = r'''
query ($filter: SerieFilter, $season: SeasonFilter, $episode: EpisodeFilter) {
     mediaSpecials(filter: $filter) {
    id
    title
    thumbnail
    media {
      duration
      videos {
        id
        url
        videoType
        bitRate
        quality
        height
        width
      }
    }
  }
  
  
}
''';

String searchMediaSpecial = r'''
query ($filter: SerieFilter) {
  mediaSpecials(filter: $filter) {
    id
    title
    thumbnail
    releaseDate
    media {
      duration
      name
      videos {
        id
        url
        videoType
        bitRate
        quality
        height
        width
      }
    }
  }
}
''';
