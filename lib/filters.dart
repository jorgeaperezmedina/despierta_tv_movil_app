import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:news_app/services/config-client.dart' as ConfigClient;
import 'package:flutter/material.dart';

String parseDateTime(String datePublish) {
  initializeDateFormatting("es_MX", null);
  try {
    // print("datePublish -> " + datePublish);
    var parseDate = ConfigClient.formateDates != ''
        ? new DateFormat(ConfigClient.formateDates).parse(datePublish)
        : datePublish;
    // var time = DateFormat("yMMMMEEEEd","es_MX").add_Hm().format(parseDate);
    var time =
        DateFormat("EEEE d 'de' MMMM yyyy, h:mm a", "es_MX").format(parseDate);
    print("time -> " + time);
    return time;
  } catch (e) {
    print(e);
    return "";
  }
}

parsetimerDuration(int duration, context, seconds) {
  var fontSize = MediaQuery.of(context).size.height <= 1000 ? 1.5 : 2.5;
  if (duration < 60) {
    return duration == 0
        ? Text(
            seconds.toString() + ' seg',
            style: TextStyle(
                // fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white),
            textScaleFactor: fontSize,
          )
        : Text(
            duration.toString() + ' min',
            style: TextStyle(
                // fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white),
            textScaleFactor: fontSize,
          );
    ;
  } else if (duration >= 60) {
    var hr = duration / 60;
    var convertHR = hr.toString().split(".");
    if (hr % 1 != 0) {
      var newHr = int.parse(convertHR[0]) * 60;
      var parseHr = duration - newHr;
      return Text(
        convertHR[0] + ' h ' + parseHr.abs().toString() + ' m ',
        style: TextStyle(
            // fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white),
        textScaleFactor: fontSize,
      );
    } else {
      return Text(
        convertHR[0] + ' hr ',
        textScaleFactor: fontSize,
        style: TextStyle(
            color: Colors.white,
            //  fontSize: 20,
            fontWeight: FontWeight.bold),
      );
    }
  }
}

Color hexToColor(String code) {
  try {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  } catch (e) {
    return Colors.grey;
  }
}

String parseDateTimeSchedules(String datePublish) {
  String formateDates = "HH:mm";
  try {
    var parseDate = formateDates != ''
        ? new DateFormat(formateDates).parse(datePublish)
        : datePublish;
    var time = DateFormat("hh:mm a").format(parseDate);
    return time;
  } catch (e) {
    print(e);
    return null;
  }
}
